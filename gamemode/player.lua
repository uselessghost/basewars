local meta = FindMetaTable( "Player" )

function meta:GetRespawnTime()
	return self:GetNWFloat( "RespawnTime", 0 )
end

function meta:SetRespawnTime( time )
	self:SetNWFloat( "RespawnTime", time )
end

function meta:IsEnemy( pl )
	return self:IsRaiding( pl ) or pl:IsRaiding( self )
end

function basewars.FindPlayer( name )
	if not name or name == "" then return end

	local pl = player.GetBySteamID( name )
	if pl then return pl end

	for k, v in ipairs( player.GetAll() ) do
		if v:Name() == name then return v end
	end

	for k, v in ipairs( player.GetAll() ) do
		if string.find( string.lower( v:Name() ), string.lower( name ) ) then return v end
	end
end

local NoClip = GetConVar( "sbox_noclip" )
function GM:PlayerNoClip( pl, enable )
	return NoClip:GetBool() or pl:IsAdmin()
end

local FriendlyFire = GetConVar( "mp_friendlyfire" )
local RDM = CreateConVar( "bw_rdm", 0, { FCVAR_NOTIFY, FCVAR_REPLICATED } )
function GM:PlayerShouldTakeDamage( pl, attacker )
	if IsValid( attacker ) and attacker:IsPlayer() and pl ~= attacker then
		if not RDM:GetBool() and not attacker:IsEnemy( pl ) then
			return false end

		if not FriendlyFire:GetBool() and pl:InFaction( attacker:Faction() ) then
			return false end
	end

	return true
end

if SERVER then
	function meta:GetOwnedEntities()
		if not self.OwnedEntities then return {} end

		for i = #self.OwnedEntities, 1, -1 do
			if not IsValid( self.OwnedEntities[ i ] ) then table.remove( self.OwnedEntities, i ) end
		end

		return self.OwnedEntities
	end

	function meta:AddOwnedEntity( ent )
		if not self.OwnedEntities then self.OwnedEntities = {} end
		table.insert( self.OwnedEntities, ent )
	end

	function meta:RemoveOwnedEntity( ent )
		if self.OwnedEntities then table.RemoveByValue( self.OwnedEntities, ent ) end
	end

	util.AddNetworkString( "Notification" )
	function meta:Notify( text, icon, duration, sound )
		net.Start( "Notification" )
			net.WriteString( text )
			net.WriteUInt( icon or 0, 8 )
			net.WriteFloat( duration or 8 )
			net.WriteString( sound or "buttons/lightswitch2.wav" )
		net.Send( self )
	end

	function meta:NotifyError( text, duration )
		self:Notify( text, 1, duration, "buttons/button10.wav" )
	end

	util.AddNetworkString( "Message" )
	function meta:Message( ... )
		local args = { ... }

		net.Start( "Message" )
			net.WriteUInt( #args, 16 )

			for k, v in ipairs( args ) do
				net.WriteType( v )
			end
		net.Send( self )
	end

	function GM:PlayerInitialSpawn( pl )	
		pl:SetTeam( TEAM_UNASSIGNED )
		pl:LoadMoney()
		basewars.SyncRaids( pl )
	end

	function GM:PlayerSpawn( pl )
		player_manager.SetPlayerClass( pl, "player_basewars" )

		pl:UnSpectate()
		pl:SetupHands()

		player_manager.OnPlayerSpawn( pl )
		player_manager.RunClass( pl, "Spawn" )

		hook.Call( "PlayerLoadout", self, pl )
		hook.Call( "PlayerSetModel", self, pl )

		local ef = EffectData()
		ef:SetEntity( pl )
		util.Effect( "PropSpawn", ef, true, true )

		if IsValid( pl:GetActiveWeapon() ) then
			ef:SetEntity( pl:GetActiveWeapon() )
			util.Effect( "PropSpawn", ef, true, true )
		end
	end

	local RespawnTime = CreateConVar( "sv_respawntime", 10, FCVAR_NOTIFY )
	function GM:PlayerDeath( victim, inflictor, attacker )
		self.BaseClass:PlayerDeath( victim, inflictor, attacker )
		victim:SetRespawnTime( CurTime() + RespawnTime:GetFloat() )
	end

	function GM:PlayerDeathThink( pl )
		if pl:GetRespawnTime() > CurTime() then return false end

		if pl:KeyDown( IN_ATTACK ) or pl:KeyDown( IN_JUMP ) or pl:IsBot() then
			pl:Spawn()
		end
	end

	function GM:PlayerDisconnected( pl )
		self:RaidDisconnect( pl )
		self:FactionDisconnect( pl )
	end

	local PropCost = CreateConVar( "bw_prop_cost", 0, FCVAR_NOTIFY )
	function GM:PlayerSpawnProp( pl, model )
		if #pl:GetRaids() > 0 then
			pl:NotifyError( "You cannot create props while in a raid." )
			return false
		end

		if not pl:CanAfford( PropCost:GetInt() ) then
			pl:NotifyError( "You can't afford to create a new prop." )
			return false
		end

		return self.BaseClass:PlayerSpawnProp( pl, model )
	end

	function GM:PlayerSpawnedProp( pl, model, ent )
		self.BaseClass:PlayerSpawnedProp( pl, model, ent )

		if PropCost:GetInt() > 0 then
			pl:Notify( string.format( "You paid $%s to spawn a prop.", string.Comma( PropCost:GetInt() ) ) )
			pl:TakeMoney( PropCost:GetInt() )
		end

		ent:SetCreator( pl )
	end

	function GM:PlayerSpawnSENT( pl, ent_class )
		return pl:IsAdmin() and self.BaseClass:PlayerSpawnSENT( pl, model )
	end

	function GM:PlayerSpawnSWEP( pl, wep_class )
		return pl:IsAdmin() and self.BaseClass:PlayerSpawnSWEP( pl, model )
	end

	function GM:PlayerGiveSWEP( pl, wep_class )
		return pl:IsAdmin() and self.BaseClass:PlayerGiveSWEP( pl, model )
	end

	function GM:PlayerSpawnVehicle( pl, model )
		return pl:IsAdmin() and self.BaseClass:PlayerSpawnVehicle( pl, model )
	end

	basewars.AddCommand( "dropweapon", function( pl, args )
		local wep = pl:GetActiveWeapon()
		if not IsValid( wep ) then return end

		local ent = ents.Create( "bw_weapon" )
		ent:SetPos( pl:EyePos() + pl:GetAimVector() * 16 )
		ent:SetModel( wep:GetModel() )
		ent:SetWeaponClass( wep:GetClass() )
		ent:Spawn()

		pl:StripWeapon( wep:GetClass() )
	end )
else
	net.Receive( "Notification", function()
		local text = net.ReadString()
		local icon = net.ReadUInt( 8 )
		local duration = net.ReadFloat()
		local sound = net.ReadString()

		notification.AddLegacy( text, icon, duration )
		if sound ~= "" then surface.PlaySound( sound ) end
	end )

	net.Receive( "Message", function()
		local args = {}

		for i = 1, net.ReadUInt( 16 ) do
			table.insert( args, net.ReadType() )
		end

		chat.AddText( color_white, unpack( args ) )
	end )
end