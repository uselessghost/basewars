basewars = {}

include( "sv_commands.lua" )

include( "shared.lua" )

AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "cl_skin.lua" )
AddCSLuaFile( "cl_hud.lua" )
AddCSLuaFile( "cl_popovers.lua" )

AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "player.lua" )
AddCSLuaFile( "entity.lua" )
AddCSLuaFile( "money.lua" )
AddCSLuaFile( "store.lua" )
AddCSLuaFile( "raids.lua" )
AddCSLuaFile( "factions.lua" )

AddCSLuaFile( "vgui/BasewarsStore.lua" )
AddCSLuaFile( "vgui/BasewarsStoreItem.lua" )
AddCSLuaFile( "vgui/BasewarsScanMenu.lua" )
AddCSLuaFile( "vgui/BasewarsFactionsMenu.lua" )
AddCSLuaFile( "vgui/BasewarsCreateFaction.lua" )

AddCSLuaFile( "settings/buyables.lua" )

resource.AddFile( "materials/basewars/armor.png" )
resource.AddFile( "materials/basewars/health.png" )
resource.AddFile( "materials/basewars/money.png" )