AddCSLuaFile()

local CLASS = {}

CLASS.WalkSpeed = 160
CLASS.RunSpeed = 350

CLASS.MaxHealth = 100
CLASS.StartHealth = 100
CLASS.StartArmor = 0

CLASS.TeammateNoCollide	= true

function CLASS:Loadout()
	self.Player:RemoveAllAmmo()

	self.Player:GiveAmmo( 256, "Pistol", true )
	self.Player:GiveAmmo( 128, "SMG1", true )
	self.Player:GiveAmmo( 4, "grenade", true )
	self.Player:GiveAmmo( 64, "Buckshot", true )
	self.Player:GiveAmmo( 32, "357", true )
	self.Player:GiveAmmo( 24, "XBowBolt", true )
	self.Player:GiveAmmo( 4, "AR2AltFire", true )
	self.Player:GiveAmmo( 128, "AR2", true )
	
	self.Player:Give( "gmod_tool" )
	self.Player:Give( "gmod_camera" )
	self.Player:Give( "weapon_physgun" )
	self.Player:Give( "weapon_physcannon" )

	self.Player:SwitchToDefaultWeapon()
end

player_manager.RegisterClass( "player_basewars", CLASS, "player_sandbox" )