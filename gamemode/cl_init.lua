basewars = {}

include( "shared.lua" )

include( "cl_skin.lua" )
include( "cl_hud.lua" )
include( "cl_popovers.lua" )

include( "vgui/BasewarsStore.lua" )
include( "vgui/BasewarsStoreItem.lua" )
include( "vgui/BasewarsScanMenu.lua" )
include( "vgui/BasewarsFactionsMenu.lua" )
include( "vgui/BasewarsCreateFaction.lua" )

local Halos = CreateConVar( "bw_halos", 1, FCVAR_ARCHIVE )

function GM:PreDrawHalos()
	if not Halos:GetBool() then return end

	self:DrawPopoverHalos()
end