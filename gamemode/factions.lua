local meta = FindMetaTable( "Player" )

function meta:Faction()
	if self:Team() > 0 and self:Team() < 1000 then
		return self:Team()
	end

	return nil
end

function meta:SetFaction( faction )
	self:SetTeam( faction or TEAM_UNASSIGNED )
end

function meta:InFaction( faction )
	return faction ~= nil and self:Faction() == faction
end

local Factions = {}

function basewars.GetFactions()
	return Factions
end

function basewars.GetFaction( faction )
	return Factions[ faction ]
end

function basewars.GetFactionByName( name )
	for k, v in pairs( Factions ) do
		if v.Name == name then return v end
	end
end

function basewars.GetFactionName( faction )
	if Factions[ faction ] then return Factions[ faction ].Name end
end

function basewars.GetFactionColor( faction )
	if Factions[ faction ] then return Factions[ faction ].Color end
end

function basewars.GetFactionFounder( faction )
	if Factions[ faction ] then return Factions[ faction ].Founder end
end

function basewars.GetFactionPassword( faction )
	if Factions[ faction ] then return Factions[ faction ].Password end
end

function basewars.SetFactionPassword( faction, password )
	if Factions[ faction ] then Factions[ faction ].Password = password end
end

function basewars.GetFactionPlayers( faction )
	return team.GetPlayers( faction )
end

if SERVER then util.AddNetworkString( "CreateFaction" ) end
function basewars.CreateFaction( founder, name, color, password, id )
	local id = id or #Factions + 1

	color.a = 255

	Factions[ id ] = {
		Founder = founder,
		Name = name,
		Color = color,
		Password = password
	}

	team.SetUp( id, name, color )

	if SERVER then
		net.Start( "CreateFaction" )
			net.WriteUInt( id, 16 )
			net.WriteEntity( founder )
			net.WriteString( name )
			net.WriteColor( color )
			net.WriteBool( password ~= "" )
		net.Broadcast()
	end

	return id
end

if SERVER then util.AddNetworkString( "DisbandFaction" ) end
function basewars.DisbandFaction( faction )
	Factions[ faction ] = nil
	team.GetAllTeams()[ faction ] = nil

	if SERVER then
		for _, pl in ipairs( basewars.GetFactionPlayers( faction ) ) do
			pl:Notify( "Your faction has been disbanded." )
			pl:SetFaction( nil )
		end

		net.Start( "DisbandFaction" )
			net.WriteUInt( faction, 16 )
		net.Broadcast()
	end
end

if SERVER then
	function GM:PlayerCanCreateFaction( pl, name, color, password )
		return true
	end

	function GM:PlayerCanJoinFaction( pl, faction )
		return true
	end

	function GM:PlayerCreatedFaction( pl, faction )
	end

	function GM:PlayerJoinedFaction( pl, faction )
	end

	function GM:PlayerLeftFaction( pl, faction )
	end

	function GM:PlayerDisbandedFaction( pl, faction )
	end

	function GM:FactionDisconnect( pl )
		pl:LeaveFaction()
	end

	function meta:CreateFaction( name, color, password )
		if self:Faction() then
			self:NotifyError( "You're already in a faction." ) return end

		if name == "" then
			self:NotifyError( "You can't create a faction with an empty name." ) return end

		if #name > 60 then
			self:NotifyError( "That name is too long." ) return end

		for k, v in pairs( Factions ) do
			if v.Name == name then
				self:NotifyError( "A faction with that name already exists." ) return end
		end

		if not gamemode.Call( "PlayerCanCreateFaction", self, name, color, password ) then return end

		local id = basewars.CreateFaction( self, name, color, password or "" )
		self:SetFaction( id )

		self:Notify( string.format( "Faction %s created.", name ) )

		gamemode.Call( "PlayerCreatedFaction", self, id )
	end

	function meta:JoinFaction( faction, password )
		if self:Faction() then
			self:NotifyError( "You're already in a faction." ) return end

		if not basewars.GetFaction( faction ) then
			self:NotifyError( "That faction no longer exists." ) return end

		if basewars.GetFactionPassword( faction ) ~= password then
			self:NotifyError( "Incorrect password." ) return end
		
		for _, pl in ipairs( basewars.GetFactionPlayers( faction ) ) do
			pl:Notify( string.format( "%s joined your faction.", self:Name() ) )
		end

		if not gamemode.Call( "PlayerCanJoinFaction", self, name, color, password ) then return end

		self:SetFaction( faction )
		self:Notify( string.format( "You joined the %s faction.", basewars.GetFactionName( faction ) ) )

		gamemode.Call( "PlayerJoinedFaction", self, id )
	end

	function meta:LeaveFaction()
		if not self:Faction() then
			self:NotifyError( "You're not in a faction." ) return end

		if self == basewars.GetFactionFounder( self:Faction() ) then
			gamemode.Call( "PlayerDisbandedFaction", self, self:Faction() )
			basewars.DisbandFaction( self:Faction() )
			return
		end

		self:SetFaction( nil )

		for _, pl in ipairs( basewars.GetFactionPlayers( self:Faction() ) ) do
			pl:Notify( string.format( "%s left your faction.", self:Name() ) )
		end

		self:Notify( "You left your faction." )

		gamemode.Call( "PlayerLeftFaction", self, id )
	end

	net.Receive( "CreateFaction", function( len, pl )
		local name = net.ReadString()
		local color = net.ReadColor()
		local password = net.ReadString()

		pl:CreateFaction( name, color, password )
	end )

	util.AddNetworkString( "JoinFaction" )
	net.Receive( "JoinFaction", function( len, pl )
		local id = net.ReadUInt( 16 )
		local password = net.ReadString()

		pl:JoinFaction( id, password )
	end )

	util.AddNetworkString( "LeaveFaction" )
	net.Receive( "LeaveFaction", function( len, pl )
		pl:LeaveFaction()
	end )

	util.AddNetworkString( "UpdateFactionPassword" )
	net.Receive( "UpdateFactionPassword", function( len, pl )
		if not pl:Faction() or basewars.GetFactionFounder( pl:Faction() ) ~= pl then return end

		basewars.SetFactionPassword( pl:Faction(), net.ReadString() )
		pl:Notify( "Faction password updated." )

		net.Start( "CreateFaction" )
			net.WriteUInt( pl:Faction(), 16 )
			net.WriteEntity( basewars.GetFactionFounder( pl:Faction() ) )
			net.WriteString( basewars.GetFactionName( pl:Faction() ) )
			net.WriteColor( basewars.GetFactionColor( pl:Faction() ) )
			net.WriteBool( basewars.GetFactionPassword( pl:Faction() ) ~= "" )
		net.Broadcast()
	end )

	util.AddNetworkString( "KickFactionMember" )
	net.Receive( "KickFactionMember", function( len, pl )
		if not pl:Faction() or basewars.GetFactionFounder( pl:Faction() ) ~= pl then return end

		local target = net.ReadEntity()
		if not IsValid( target ) or not target:IsPlayer() or target:Faction() ~= pl:Faction() then
			return end

		if target == pl then pl:NotifyError( "You can't kick yourself." ) return end

		target:SetFaction( nil )
		target:NotifyError( "You were kicked from your faction." )

		pl:Notify( "Faction member kicked." )

		gamemode.Call( "PlayerLeftFaction", self, id )
	end )

	function GM:ShowTeam( pl )
		pl:ConCommand( "bw_factions" )
	end

	basewars.AddCommand( "factions", function( pl )
		pl:ConCommand( "bw_factions" )
	end )

	concommand.Add( "bw_syncfactions", function( pl, cmd, args )
		for k, v in pairs( Factions ) do
			net.Start( "CreateFaction" )
				net.WriteUInt( k, 16 )
				net.WriteEntity( v.Founder )
				net.WriteString( v.Name )
				net.WriteColor( v.Color )
				net.WriteBool( v.Password ~= "" )
			net.Broadcast()
		end
	end )
else
	hook.Add( "InitPostEntity", "SyncFactions", function()
		RunConsoleCommand( "bw_syncfactions" )
	end )

	concommand.Add( "bw_factions", function()
		local p = vgui.Create( "BasewarsFactionsMenu" )
		p:SetSize( 800, 500 )
		p:Center()
		p:MakePopup()
	end )

	concommand.Add( "bw_createfaction", function()
		local p = vgui.Create( "BasewarsCreateFaction" )
		p:SetSize( 300, 400 )
		p:Center()
		p:MakePopup()
	end )

	net.Receive( "CreateFaction", function()
		local id = net.ReadUInt( 16 )
		local founder = net.ReadEntity()
		local name = net.ReadString()
		local color = net.ReadColor()
		local passworded = net.ReadBool()

		basewars.CreateFaction( founder, name, color, passworded, id )
	end )

	net.Receive( "DisbandFaction", function()
		basewars.DisbandFaction( net.ReadUInt( 16 ) )
	end )
end