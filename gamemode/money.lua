local meta = FindMetaTable( "Player" )

function meta:GetMoney()
	return self:GetNWInt( "Money", 0 )
end

function meta:SetMoney( money )
	money = math.Clamp( money, 0, 2 ^ 32 / 2 - 1 )
	self:SetNWInt( "Money", money )
	self:SetPData( "BasewarsMoney", money )
end

function meta:GiveMoney( money )
	self:SetMoney( self:GetMoney() + money )
end

function meta:TakeMoney( money )
	self:SetMoney( self:GetMoney() - money )
end

function meta:CanAfford( price )
	return self:GetMoney() >= price
end

local StartingMoney = CreateConVar( "bw_money_starting", 5000, FCVAR_NOTIFY )
function meta:LoadMoney()
	self:SetMoney( tonumber( self:GetPData( "BasewarsMoney" ) ) or StartingMoney:GetInt() )
end

if CLIENT then return end

basewars.AddCommand( "dropmoney", function( pl, args )
	local amount = math.max( tonumber( args[ 1 ] or 100 ), 100 )

	if not pl:CanAfford( amount ) then
		pl:NotifyError( "You can't afford to drop that much money." ) return end

	local tr = util.TraceLine{
		start = pl:EyePos(),
		endpos = pl:EyePos() + pl:GetAimVector() * 64,
		filter = pl
	}

	local ent = ents.Create( "bw_money" )
	ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
	ent:SetAmount( amount )
	ent:Spawn()

	pl:TakeMoney( amount )

	pl:Notify( string.format( "You dropped $%s.", string.Comma( amount ) ) )
end )

basewars.AddCommand( "givemoney", function( pl, args )
	local amount = tonumber( args[ 1 ] or 100 )

	if not pl:CanAfford( amount ) then
		pl:NotifyError( "You can't afford to give that much money." ) return end

	local tr = util.TraceLine{
		start = pl:EyePos(),
		endpos = pl:EyePos() + pl:GetAimVector() * 256,
		filter = pl
	}

	if not IsValid( tr.Entity ) or not tr.Entity:IsPlayer() then
		pl:NotifyError( "You aren't looking at a player." ) return end

	tr.Entity:GiveMoney( amount )
	pl:TakeMoney( amount )

	pl:Notify( string.format( "You gave $%s to %s.", string.Comma( amount ), tr.Entity:Name() ) )
	tr.Entity:Notify( string.format( "You received $%s from %s.", string.Comma( amount ), pl:Name() ) )
end )

local PaydayPayout = CreateConVar( "bw_payday_payout", 500, FCVAR_NOTIFY )
function meta:Payday()
	self:Notify( string.format( "Payday! You received $%s.", string.Comma( PaydayPayout:GetInt() ) ) )
	self:GiveMoney( PaydayPayout:GetInt() )
end

local PaydayDelay = CreateConVar( "bw_payday_delay", 600, FCVAR_NOTIFY )

local function CreatePaydayTimer()
	timer.Create( "Payday", PaydayDelay:GetFloat(), 0, function()
		for k, v in ipairs( player.GetAll() ) do
			v:Payday()
		end
	end )
end

cvars.AddChangeCallback( "bw_payday_delay", function( convar, old, new )
	timer.Destroy( "Payday" )
	CreatePaydayTimer()
end )

CreatePaydayTimer()