-- Health + Armor
basewars.AddBuyable( "bw_medkit_large", {
	Name = "Large Medkit",
	Category = "Restoration",
	Model = Model( "models/Items/HealthKit.mdl" ),
	Price = 100,
} )

basewars.AddBuyable( "bw_medkit_small", {
	Name = "Small Medkit",
	Category = "Restoration",
	Model = Model( "models/healthvial.mdl" ),
	Price = 50,
} )

basewars.AddBuyable( "bw_armor_large", {
	Name = "Large Armor",
	Category = "Restoration",
	Model = Model( "models/items/battery.mdl" ),
	Price = 100,
} )

basewars.AddBuyable( "bw_armor_small", {
	Name = "Small Armor",
	Category = "Restoration",
	Model = Model( "models/items/battery.mdl" ),
	Price = 50,
} )

-- Ammo
basewars.AddAmmo( "bw_ammo_pistol", {
	Name = "Pistol Ammo",
	Category = "Ammunition",
	Model = Model( "models/Items/BoxSRounds.mdl" ),
	Price = 50,
	AmmoType = "pistol",
	Amount = 32
} )

basewars.AddAmmo( "bw_ammo_smg", {
	Name = "SMG Ammo",
	Category = "Ammunition",
	Model = Model( "models/Items/BoxMRounds.mdl" ),
	Price = 200,
	AmmoType = "smg1",
	Amount = 64
} )

basewars.AddAmmo( "bw_ammo_shotgun", {
	Name = "Shotgun Ammo",
	Category = "Ammunition",
	Model = Model( "models/Items/BoxBuckshot.mdl" ),
	Price = 200,
	AmmoType = "buckshot",
	Amount = 32
} )

basewars.AddAmmo( "bw_ammo_grenade", {
	Name = "Grenade Ammo",
	Category = "Ammunition",
	Model = Model( "models/Items/grenadeAmmo.mdl" ),
	Price = 200,
	AmmoType = "grenade",
	Amount = 10
} )

basewars.AddAmmo( "bw_ammo_rpg", {
	Name = "RPG Ammo",
	Category = "Ammunition",
	Model = Model( "models/weapons/w_missile_closed.mdl" ),
	Price = 200,
	AmmoType = "rpg_round",
	Amount = 12
} )

-- Generators
basewars.AddBuyable( "bw_fuel", {
	Name = "Fuel",
	Category = "Generators",
	Model = Model( "models/props_junk/gascan001a.mdl" ),
	Price = 500,
} )

basewars.AddBuyable( "bw_generator_small", {
	Name = "Small Generator",
	Category = "Generators",
	Model = Model( "models/props_c17/trappropeller_engine.mdl" ),
	Price = 1000,
} )

basewars.AddBuyable( "bw_generator_large", {
	Name = "Large Generator",
	Category = "Generators",
	Model = Model( "models/props_c17/FurnitureBoiler001a.mdl" ),
	Price = 10000,
} )

basewars.AddBuyable( "bw_generator_nuclear", {
	Name = "Nuclear Generator",
	Category = "Generators",
	Model = Model( "models/props_wasteland/laundry_washer003.mdl" ),
	Price = 1000000,
	Icons = { "icon16/error.png" }
} )

-- Printers
basewars.AddBuyable( "bw_paper", {
	Name = "Printer Paper",
	Category = "Printers",
	Model = Model( "models/props_junk/garbage_newspaper001a.mdl" ),
	Price = 500,
} )

basewars.AddBuyable( "bw_printer_bronze", {
	Name = "Bronze Printer",
	Category = "Printers",
	Model = Model( "models/props_lab/reciever01a.mdl" ),
	Price = 1000,
	Icons = { "icon16/lightning.png" }
} )

basewars.AddBuyable( "bw_printer_silver", {
	Name = "Silver Printer",
	Category = "Printers",
	Model = Model( "models/props_lab/reciever01a.mdl" ),
	Price = 5000,
	Icons = { "icon16/lightning.png" }
} )

basewars.AddBuyable( "bw_printer_gold", {
	Name = "Gold Printer",
	Category = "Printers",
	Model = Model( "models/props_lab/reciever01a.mdl" ),
	Price = 25000,
	Icons = { "icon16/lightning.png", "icon16/error.png" }
} )

basewars.AddBuyable( "bw_printer_diamond", {
	Name = "Diamond Printer",
	Category = "Printers",
	Model = Model( "models/props_c17/consolebox01a.mdl" ),
	Price = 130000,
	Icons = { "icon16/lightning.png", "icon16/error.png" }
} )

basewars.AddBuyable( "bw_printer_obsidian", {
	Name = "Obsidian Printer",
	Category = "Printers",
	Model = Model( "models/props_c17/consolebox01a.mdl" ),
	Price = 650000,
	Icons = { "icon16/lightning.png", "icon16/error.png" }
} )

-- Structures
basewars.AddBuyable( "bw_ammomanufacturer", {
	Name = "Ammo Manufacturer",
	Category = "Structures",
	Model = Model( "models/props_lab/reciever_cart.mdl" ),
	Price = 10000,
	Icons = { "icon16/lightning.png" }
} )

basewars.AddBuyable( "bw_healthregenerator", {
	Name = "Health Regenerator",
	Category = "Structures",
	Model = Model( "models/props_combine/health_charger001.mdl" ),
	Price = 5000,
	Icons = { "icon16/lightning.png" }
} )

basewars.AddBuyable( "bw_armorregenerator", {
	Name = "Armor Regenerator",
	Category = "Structures",
	Model = Model( "models/props_combine/suit_charger001.mdl" ),
	Price = 5000,
	Icons = { "icon16/lightning.png" }
} )

basewars.AddBuyable( "bw_moneyvault", {
	Name = "Money Vault",
	Category = "Structures",
	Model = Model( "models/props_wasteland/kitchen_fridge001a.mdl" ),
	Price = 500,
} )

basewars.AddBuyable( "bw_scanner", {
	Name = "Scanner",
	Category = "Structures",
	Model = Model( "models/props_rooftop/satellitedish02.mdl" ),
	Price = 2500,
} )

basewars.AddBuyable( "bw_spawn", {
	Name = "Spawnpoint",
	Category = "Structures",
	Model = Model( "models/props_combine/combine_mine01.mdl" ),
	Price = 500,
	Spawn = function( self, pl )
		local ent = ents.Create( self.Class )
		ent:SetPos( pl:GetPos() )
		ent:Spawn()
		ent:Activate()

		return ent
	end 
} )

-- Turrets
basewars.AddBuyable( "bw_turret_basic", {
	Name = "Basic Turret",
	Category = "Self Defense",
	Model = Model( "models/combine_turrets/floor_turret.mdl" ),
	Price = 500,
} )

basewars.AddBuyable( "bw_turret_plasma", {
	Name = "Plasma Turret",
	Category = "Self Defense",
	Model = Model( "models/combine_turrets/floor_turret.mdl" ),
	Price = 5000,
} )

basewars.AddBuyable( "bw_turret_laser", {
	Name = "Laser Turret",
	Category = "Self Defense",
	Model = Model( "models/combine_turrets/floor_turret.mdl" ),
	Price = 50000,
} )

basewars.AddBuyable( "bw_tesla", {
	Name = "Tesla Coil",
	Category = "Self Defense",
	Model = Model( "models/props_c17/utilityconnecter006c.mdl" ),
	Price = 50000,
} )

-- Powerups
basewars.AddBuyable( "bw_scanblocker", {
	Name = "Scan Blocker",
	Category = "Powerups",
	Model = Model( "models/items/car_battery01.mdl" ),
	Price = 100000,
} )

-- Weapons
basewars.AddWeapon( "bw_welder", {
	Name = "Welder",
	Category = "Weapons - Utility",
	Model = Model( "models/weapons/w_irifle.mdl" ),
	Price = 1000,
} )

basewars.AddWeapon( "lite_ak47", {
	Name = "AK-47",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_rif_ak47.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_awp", {
	Name = "AWP",
	Category = "Weapons - Snipers",
	Model = Model( "models/weapons/w_snip_awp.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_dualberettas", {
	Name = "Dual 96G Elite Berettas",
	Category = "Weapons - Side arms",
	Model = Model( "models/weapons/w_pist_elite.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_galil", {
	Name = "Galil",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_rif_galil.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_m3", {
	Name = "M3 Super 90",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_shot_m3super90.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_aug", {
	Name = "AUG",
	Category = "Weapons - Snipers",
	Model = Model( "models/weapons/w_rif_aug.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_p228", {
	Name = "P228",
	Category = "Weapons - Side arms",
	Model = Model( "models/weapons/w_pist_p228.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_m4a1", {
	Name = "M4A1",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_rif_m4a1.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_xm1014", {
	Name = "XM1014",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_shot_xm1014.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_p90", {
	Name = "P90",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_smg_p90.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_tmp", {
	Name = "TMP",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_smg_tmp.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_deagle", {
	Name = "Desert Eagle",
	Category = "Weapons - Side arms",
	Model = Model( "models/weapons/w_pist_deagle.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_famas", {
	Name = "FAMAS",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_rif_famas.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_scout", {
	Name = "Scout",
	Category = "Weapons - Snipers",
	Model = Model( "models/weapons/w_snip_scout.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_ump", {
	Name = "UMP",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_smg_ump45.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_fiveseven", {
	Name = "FN Five-seveN",
	Category = "Weapons - Side arms",
	Model = Model( "models/weapons/w_pist_fiveseven.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_m249", {
	Name = "M249",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_mach_m249para.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_g3sg1", {
	Name = "G3/SG-1",
	Category = "Weapons - Snipers",
	Model = Model( "models/weapons/w_snip_g3sg1.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_sg550", {
	Name = "SG-550",
	Category = "Weapons - Snipers",
	Model = Model( "models/weapons/w_snip_sg550.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_usp", {
	Name = "USP",
	Category = "Weapons - Side arms",
	Model = Model( "models/weapons/w_pist_usp.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_mac10", {
	Name = "MAC10",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_smg_mac10.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_sg552", {
	Name = "SG-552",
	Category = "Weapons - Snipers",
	Model = Model( "models/weapons/w_rif_sg552.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_glock", {
	Name = "Glock 18",
	Category = "Weapons - Side arms",
	Model = Model( "models/weapons/w_pist_glock18.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_mp5", {
	Name = "MP5",
	Category = "Weapons - Primary",
	Model = Model( "models/weapons/w_smg_mp5.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_hegrenade", {
	Name = "HE Grenade",
	Category = "Weapons - Grenades",
	Model = Model( "models/weapons/w_eq_fraggrenade.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_smokegrenade", {
	Name = "Smoke Grenade",
	Category = "Weapons - Grenades",
	Model = Model( "models/weapons/w_eq_smokegrenade.mdl" ),
	Price = 100,
} )

basewars.AddWeapon( "lite_flashbang", {
	Name = "Flashbang",
	Category = "Weapons - Grenades",
	Model = Model( "models/weapons/w_eq_flashbang.mdl" ),
	Price = 100,
} )

