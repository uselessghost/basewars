local meta = FindMetaTable( "Entity" )

function meta:GetCreator()
	return self:GetNWEntity( "Creator" )
end

function meta:SetCreator( pl )
	self:SetNWEntity( "Creator", pl )
end

function GM:EntityRemoved( ent )
	if SERVER and ent.Raidable then self:RaidEntityDestroyed( ent ) end
end

if SERVER then
	basewars.AddCommand( "upgrade", function( pl, args )
		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 256,
			filter = pl
		}

		local ent = tr.Entity
		if not IsValid( ent ) then return end

		if ent:GetCreator() ~= pl then
			pl:NotifyError( "You do not own this entity." ) return end

		if not ent.Upgradable then
			pl:NotifyError( "This entity is not upgradable." ) return end

		ent:Upgrade( pl )
	end )

	basewars.AliasCommand( "upg", "upgrade" )
end