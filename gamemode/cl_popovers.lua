local Entities = {}
local Materials = {}

local function CachedMaterial( mat )
	if not Materials[ mat ] then
		Materials[ mat ] = Material( mat )
	end

	return Materials[ mat ]
end

local GaugeFullColor = Color( 30, 255, 30 )
local GaugeEmptyColor = Color( 255, 30, 30 )

local PopoverHandlers = {
	header = function( params, x, y, w, h )
		draw.RoundedBox( 2, x, y, w, h, Color( 44, 62, 80 ) )

		draw.SimpleTextOutlined( params.Text, params.Font or "DermaDefaultBold", x + w / 2, y + h / 2,
			color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
	end,

	text = function( params, x, y, w, h )
		draw.SimpleTextOutlined( params.Text, params.Font or "DermaDefault", x + w / 2, y + h / 2,
			color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
	end,

	gauge = function( params, x, y, w, h )
		local mul = params.Value / params.Maximum

		local col = Color( 0, 0, 0 )
		col.r = Lerp( mul, GaugeEmptyColor.r, GaugeFullColor.r )
		col.g = Lerp( mul, GaugeEmptyColor.g, GaugeFullColor.g )
		col.b = Lerp( mul, GaugeEmptyColor.b, GaugeFullColor.b )

		GAMEMODE:DrawProgressBar( x, y, w, h, mul, col, params.Text, "DermaDefault", true )
	end
}

function GM:DrawPopovers()
	local tr = util.TraceLine{
		start = LocalPlayer():EyePos(),
		endpos = LocalPlayer():EyePos() + LocalPlayer():GetAimVector() * 128,
		filter = LocalPlayer()
	}

	if IsValid( tr.Entity ) and not Entities[ tr.Entity ] and tr.Entity.GetPopover then
		Entities[ tr.Entity ] = 0
	end

	for k, v in pairs( Entities ) do
		if IsValid( k ) then
			local info = k:GetPopover()

			table.insert( info, 1, {
				Type = "header",
				Icon = k.InfoIcon or "icon16/bricks.png",
				Text = language.GetPhrase( k:GetClass() )
			} )

			if IsValid( k:GetCreator() ) then
				table.insert( info, 2, {
					Type = "text",
					Icon = "icon16/user.png",
					Text = "Owned by: " .. k:GetCreator():Name()
				} )
			end

			local screenpos = k:LocalToWorld( k:OBBCenter() ):ToScreen()

			local w, h = 250, 10 + #info * 26 - 2
			local x, y = math.floor( screenpos.x - w / 2 ), math.floor( screenpos.y - h - 16 )

			surface.SetAlphaMultiplier( v )

			self:DrawRect( x, y, w, h )

			--draw.RoundedBox( 2, x, y, w, h, Color( 0, 0, 0, 200 ) )

			draw.NoTexture()
			surface.DrawPoly{
				{ x = x + w / 2 - 16, y = y + h },
				{ x = x + w / 2 + 16, y = y + h },
				{ x = x + w / 2,      y = y + h + 16 },
			}

			for k, v in ipairs( info ) do
				local el_w, el_h = w - 10, 24
				local el_x, el_y = x + 5, y + 5 + ( k - 1 ) * ( el_h + 2 )

				PopoverHandlers[ v.Type ]( v, el_x, el_y, el_w, el_h )

				if v.Icon then
					GAMEMODE:DrawIcon( el_x + 5, el_y + el_h / 2 - 8, CachedMaterial( v.Icon ) )
				end
			end

			surface.SetAlphaMultiplier( 1 )

			Entities[ k ] = Lerp( FrameTime() * 10, v, tr.Entity == k and 1 or 0 )
			if Entities[ k ] <= 0.001 then
				Entities[ k ] = nil
			end
		else
			Entities[ k ] = nil
		end
	end
end

function GM:DrawPopoverHalos()
	for k, v in pairs( Entities ) do
		if v > 0.001 then
			halo.Add( { k }, Color( 100, 200, 255, 255 * v ), 2 * v, 2 * v )
		end
	end
end