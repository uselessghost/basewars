local Gradient = Material( "gui/gradient_down" )
local MoneyIcon = Material( "icon16/money.png" )

function GM:DrawRect( x, y, w, h )
	surface.SetDrawColor( Color( 0, 0, 0, 200 ) )
	surface.DrawRect( x, y, w, h )
end

function GM:DrawProgressBar( x, y, w, h, filled, color, text, text_font, background )
	if background then self:DrawRect( x, y, w, h ) end

	if filled > 0 then

		filled = math.Clamp( filled, 0, 1 )

		local base_color = Color( color.r / 2, color.g / 2, color.b / 2, color.a )
		surface.SetDrawColor( base_color )
		surface.DrawRect( x, y, w * filled, h )
		
		surface.SetMaterial( Gradient )
		surface.SetDrawColor( color )
		surface.DrawTexturedRect( x, y, w * filled, h )
		
		surface.SetDrawColor( Color( 0, 0, 0, 200 ) )
		surface.DrawOutlinedRect( x, y, w * filled, h )
	end

	if text then
		draw.SimpleTextOutlined( text, text_font or "BasewarsHUD", x + w / 2, y + h / 2, color_white,
			TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
	end
end

function GM:DrawIcon( x, y, icon )
	surface.SetMaterial( icon )
	surface.SetDrawColor( color_white )
	surface.DrawTexturedRect( x, y, 16, 16 )
end

surface.CreateFont( "BasewarsHUD", {
	font = "Roboto Condensed",
	size = 24
} )

local Heart = Material( "basewars/health.png", "smooth" )
local Shield = Material( "basewars/armor.png", "smooth" )
local Money = Material( "basewars/money.png", "smooth" )

local HealthFullColor = Color( 30, 255, 30 )
local HealthEmptyColor = Color( 255, 30, 30 )
local ArmorColor = Color( 25, 125, 245 )

local ShouldDraw = GetConVar( "cl_drawhud" )
local Health, Armor = 0, 0
function GM:DrawHUD()
	if not ShouldDraw:GetBool() then return end

	local p = 2
	local w, h = 32, 32
	local bar_w = 300
	local x, y = p, ScrH() - p
	local pl = LocalPlayer()
	
	--self:DrawRect( x, y, w, h )

	Health = Lerp( FrameTime() * 4, Health, pl:Health() )
	Armor = Lerp( FrameTime() * 4, Armor, pl:Armor() )
	
	local health_mul = math.Round( math.Clamp( Health / pl:GetMaxHealth(), 0, 1 ), 3 )
	local armor_mul = math.Round( math.Clamp( Armor / pl:GetMaxHealth(), 0, 1 ), 3 )

	local col = Color( 0, 0, 0 )
	col.r = Lerp( health_mul, HealthEmptyColor.r, HealthFullColor.r )
	col.g = Lerp( health_mul, HealthEmptyColor.g, HealthFullColor.g )
	col.b = Lerp( health_mul, HealthEmptyColor.b, HealthFullColor.b )
	
	local text = tostring( math.Round( Health ) )
	
	if math.Round( Armor ) > 0 then
		text = text .. " | " .. math.Round( Armor )
	end

	self:DrawRect( x, y - h, w, h )

	surface.SetMaterial( Shield )
	surface.SetDrawColor( color_white )
	surface.DrawTexturedRect( x + p, y - h + p, w - p * 2, h - p * 2 )

	self:DrawRect( x + w + p, y - h, bar_w, h )

	self:DrawProgressBar( x + w + p * 2, y - h + p, bar_w - p * 2, h - p * 2, armor_mul,
		ArmorColor, math.Round( Armor ) )

	local y = y - h - p
	self:DrawRect( x, y - h, w, h )

	surface.SetMaterial( Heart )
	surface.SetDrawColor( color_white )
	surface.DrawTexturedRect( x + p, y - h + p, w - p * 2, h - p * 2 )

	self:DrawRect( x + w + p, y - h, bar_w, h )

	self:DrawProgressBar( x + w + p * 2, y - h + p, bar_w - p * 2, h - p * 2, health_mul,
		col, math.Round( Health ) )

	local y = y - h - p
	self:DrawRect( x, y - h, w, h )

	surface.SetMaterial( Money )
	surface.SetDrawColor( color_white )
	surface.DrawTexturedRect( x + p, y - h + p, w - p * 2, h - p * 2 )

	self:DrawRect( x + w + p, y - h, bar_w, h )

	draw.SimpleTextOutlined( "$" .. string.Comma( pl:GetMoney() ), "BasewarsHUD", x + w + p + bar_w / 2, y - h / 2,
		color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
end

function GM:DrawRespawnTimer()
	if LocalPlayer():Alive() then return end

	local time = LocalPlayer():GetRespawnTime()

	if time > CurTime() then
		draw.SimpleTextOutlined( string.format( "Respawning in %d second(s)...", time - CurTime() ),
			"DermaLarge", ScrW() / 2, ScrH() - 150, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER,
			1, color_black )
	else
		draw.SimpleTextOutlined( "Click to respawn", "DermaLarge",
			ScrW() / 2, ScrH() - 150, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
	end
end

function GM:HUDPaint()
	self.BaseClass:HUDPaint()

	self:DrawHUD()
	self:DrawRespawnTimer()
	self:DrawPopovers()
	self:DrawRaids()
end

function GM:HUDShouldDraw( el )
	return not ( el == "CHudHealth" or el == "CHudBattery" )
end	