local Commands = {}

function basewars.AddCommand( cmd, func )
	Commands[ cmd ] = func
end

function basewars.AliasCommand( alias, cmd )
	Commands[ alias ] = Commands[ cmd ]
end

function basewars.RemoveCommand( cmd )
	Commands[ cmd ] = nil
end

function basewars.CommandExists( cmd )
	return Commands[ cmd ] ~= nil
end

function basewars.RunCommand( pl, cmd, args )
	if Commands[ cmd ] then return Commands[ cmd ]( pl, args ) end
end

function GM:PlayerSay( pl, text, team )
	local match = string.match( text, "^/(.+)$" )
	if match then
		local args = string.Explode( " ", match )
		local cmd = table.remove( args, 1 )

		if not basewars.CommandExists( cmd ) then return end
		
		basewars.RunCommand( pl, cmd, args )
		return ""
	end

	return text
end

concommand.Add( "bw", function( pl, cmd, args )
	if not IsValid( pl ) then return end

	local cmd = table.remove( args, 1 )
	if not basewars.CommandExists( cmd ) then return end

	basewars.RunCommand( pl, cmd, args )
end )