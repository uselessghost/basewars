local Buyables = {}

function basewars.GetBuyables()
	return Buyables
end

function basewars.GetBuyable( class )
	return Buyables[ class ]
end

function basewars.AddBuyable( class, opts )
	if not opts.Class then opts.Class = class end
	if not opts.Name then opts.Name = "Error: Name not defined" end
	if not opts.Category then opts.Category = "Miscellaneous" end
	if not opts.Price then opts.Price = 0 end
	if not opts.Model then opts.Model = "models/Combine_Helicopter/helicopter_bomb01.mdl" end
	if not opts.Maximum then opts.Maximum = 4 end

	Buyables[ class ] = opts
end

function basewars.AddWeapon( class, opts )
	opts.Icons = { "icon16/gun.png" }
	
	function opts:CanBuy( pl )
		if pl:HasWeapon( self.Class ) then
			pl:NotifyError( "You already have this weapon." )
			return false
		end

		return true
	end

	function opts:Spawn( pl, tr )
		pl:Give( self.Class )
		pl:SelectWeapon( self.Class )
	end

	basewars.AddBuyable( class, opts )
end

function basewars.AddAmmo( class, opts )
	opts.Icons = { "icon16/gun.png" }

	function opts:Spawn( pl, tr )
		pl:GiveAmmo( self.Amount, self.AmmoType )
	end

	basewars.AddBuyable( class, opts )
end

if SERVER then
	function GM:PlayerCanBuy( pl, buyable )
		return true
	end

	function GM:PlayerBought( pl, buyable, ent )
	end

	function GM:ModifyBuyablePrice( pl, price, buyable )
		return price
	end

	function basewars.PurchaseBuyable( pl, class )
		if pl.NextBuy and pl.NextBuy > CurTime() then return false end

		local buyable = basewars.GetBuyable( class )
		if not buyable then return false end

		if not pl:CanAfford( buyable.Price ) then
			pl:NotifyError( "You can't afford this item." )
			return false
		end

		if pl:GetCount( class ) >= buyable.Maximum then
			pl:NotifyError( "You cannot buy any more of these entities." )
			return false
		end

		if buyable.CanBuy and not buyable:CanBuy( pl ) then return false end
		if not gamemode.Call( "PlayerCanBuy", pl, buyable ) then return false end

		local tr = util.TraceLine{
			start = pl:EyePos(),
			endpos = pl:EyePos() + pl:GetAimVector() * 64,
			filter = pl
		}

		local price = buyable.Price

		if buyable.ModifyPrice then price = buyable:ModifyPrice( pl, price ) end
		price = gamemode.Call( "ModifyBuyablePrice", pl, price, buyable )

		pl:TakeMoney( buyable.Price )

		local ent
		if buyable.Spawn then 
			ent = buyable:Spawn( pl, tr )
		else
			ent = ents.Create( buyable.Class )
			ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
			ent:Spawn()
			ent:Activate()
		end

		if IsValid( ent ) then
			if CPPI then ent:CPPISetOwner( pl ) end

			ent:SetCreator( pl )
			pl:AddCount( class, ent )
			pl:AddOwnedEntity( ent )
		end

		gamemode.Call( "PlayerBought", pl, buyable, ent )
		pl.NextBuy = CurTime() + 0.5

		pl:Notify( string.format( "You purchased a(n) %s for $%s.", buyable.Name,
			string.Comma( buyable.Price ) ) )

		return ent
	end

	basewars.AddCommand( "buy", function( pl, args )
		if args[ 1 ] then basewars.PurchaseBuyable( pl, args[ 1 ] ) end
	end )
else
	spawnmenu.AddCreationTab( "Store", function()
		return vgui.Create( "BasewarsStore" )
	end, "icon16/cart.png", 10 )

	hook.Add( "PopulateContent", "RemoveIrrelevantTabs", function()
		if LocalPlayer():IsAdmin() then return end

		local tabs = {}

		for k, v in pairs( g_SpawnMenu.CreateMenu.Items ) do
			if v.Tab:GetText() ~= language.GetPhrase( "spawnmenu.content_tab" ) and 
				v.Tab:GetText() ~= "Store" then
				table.insert( tabs, v.Tab )
			end
		end

		for k, v in ipairs( tabs ) do
			g_SpawnMenu.CreateMenu:CloseTab( v, true )
		end
	end )
end