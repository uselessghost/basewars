AddCSLuaFile()

GM.Name = "BaseWars"
GM.Author = "Olivia (UselessGhost)"
GM.Email = "N/A"
GM.Website = "https://uselessghost.me/"

DeriveGamemode( "sandbox" )

include( "classes/player_basewars.lua" )

include( "player.lua" )
include( "entity.lua" )
include( "money.lua" )
include( "store.lua" )
include( "raids.lua" )
include( "factions.lua" )

include( "settings/buyables.lua" )

local plugins = "basewars/gamemode/plugins/"
local _, dirs = file.Find( plugins .. "*", "LUA" )
for _, plugin in ipairs( dirs ) do
	for _, filename in ipairs( file.Find( plugins .. plugin .. "/*", "LUA" ) ) do
		if not string.match( filename, "^sv_" ) then
			AddCSLuaFile( plugins .. plugin .. "/" .. filename )
		end
	end

	local sv_init = plugins .. plugin .. "/sv_init.lua"
	local cl_init = plugins .. plugin .. "/cl_init.lua"

	include( SERVER and sv_init or cl_init )
end