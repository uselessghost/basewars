local PANEL = {}

function PANEL:Init()	
	self:SetTitle( "Scan" )
	self:SetSkin( "flat" )

	self.PlayerList = vgui.Create( "DListView", self )
	self.PlayerList:Dock( FILL )
	self.PlayerList:DockMargin( 0, 5, 0, 5 )
	self.PlayerList:SetMultiSelect( true )
	self.PlayerList:AddColumn( "Player" )

	for _, pl in ipairs( player.GetAll() ) do
		if pl ~= LocalPlayer() then self.PlayerList:AddLine( pl:Name() ) end
	end

	self.Scan = vgui.Create( "DButton", self )
	self.Scan:Dock( BOTTOM )
	self.Scan:SetText( "Scan" )
	self.Scan:SetImage( "icon16/transmit.png" )

	function self.Scan.DoClick()
		for k, v in ipairs( self.PlayerList:GetSelected() ) do
			RunConsoleCommand( "bw", "scan", v:GetColumnText( 1 ) )
		end

		self:Close()
	end
end

vgui.Register( "BasewarsScanMenu", PANEL, "DFrame" )