local PANEL = {}

function PANEL:Init()
	self:SetTitle( "Factions" )
	self:SetSkin( "flat" )

	self.LeftPanel = vgui.Create( "Panel", self )
	self.LeftPanel:Dock( LEFT )
	self.LeftPanel:SetWide( 200 )

	self.FactionList = vgui.Create( "DTree", self.LeftPanel )
	self.FactionList:Dock( FILL )

	for k, v in pairs( basewars.GetFactions() ) do
		local icon = v.Password and "icon16/lock.png" or "icon16/lock_open.png"
		local node = self.FactionList:AddNode( v.Name )
		node:SetIcon( icon )

		function node.DoClick()
			self:ViewFaction( k )
		end
	end

	self.CreateFaction = vgui.Create( "DButton", self.LeftPanel )
	self.CreateFaction:Dock( BOTTOM )
	self.CreateFaction:DockMargin( 0, 5, 0, 0 )
	self.CreateFaction:SetText( "Create Faction" )
	self.CreateFaction:SetImage( "icon16/group_add.png" )

	function self.CreateFaction.DoClick()
		self:Close()
		RunConsoleCommand( "bw_createfaction" )
	end

	self.RightPanel = vgui.Create( "DPanel", self )
	self.RightPanel:Dock( FILL )
	self.RightPanel:DockMargin( 5, 0, 0, 0 )
	self.RightPanel:DockPadding( 5, 5, 5, 5 )

	self.FactionName = vgui.Create( "DLabel", self.RightPanel )
	self.FactionName:Dock( TOP )
	self.FactionName:SetText( "Select a faction" )
	self.FactionName:SetFont( "DermaLarge" )
	self.FactionName:SetDark( true )
	self.FactionName:SizeToContents()

	self.FactionMembers = vgui.Create( "DListView", self.RightPanel )
	self.FactionMembers:Dock( FILL )
	self.FactionMembers:DockMargin( 0, 5, 0, 5 )
	local column = self.FactionMembers:AddColumn( "" )
	column:SetMaxWidth( 16 )
	column:SetMinWidth( 16 )
	self.FactionMembers:AddColumn( "Player" )
	self.FactionMembers:AddColumn( "Kills" ):SetMaxWidth( 90 )
	self.FactionMembers:AddColumn( "Deaths" ):SetMaxWidth( 90 )

	function self.FactionMembers:OnClickLine( line, selected )
		local pl = line.Player
		if not IsValid( pl ) or pl == LocalPlayer() then return end

		if LocalPlayer():Faction() ~= pl:Faction() or
			basewars.GetFactionFounder( pl:Faction() ) ~= LocalPlayer() then return end

		local menu = DermaMenu()
		menu:AddOption( "Kick", function()
			line:Remove()

			net.Start( "KickFactionMember" )
				net.WriteEntity( pl )
			net.SendToServer()
		end ):SetIcon( "icon16/user_delete.png" )
		menu:Open()
	end

	self.Toolbar = vgui.Create( "Panel", self.RightPanel )
	self.Toolbar:Dock( BOTTOM )

	self.Join = vgui.Create( "DButton", self.Toolbar )
	self.Join:Dock( LEFT )
	self.Join:DockMargin( 0, 0, 1, 0 )
	self.Join:SetText( "Join Faction" )
	self.Join:SetImage( "icon16/group_go.png" )
	self.Join:SetWide( 150 )
	self.Join:SetEnabled( false )

	self.UpdatePassword = vgui.Create( "DButton", self.Toolbar )
	self.UpdatePassword:Dock( RIGHT )
	self.UpdatePassword:SetText( "Update Password" )
	self.UpdatePassword:SetImage( "icon16/lock_edit.png" )
	self.UpdatePassword:SetWide( 150 )
	self.UpdatePassword:SetEnabled( false )

	function self.UpdatePassword.DoClick()
		Derma_StringRequest( "Update faction password", "Leave empty to set no password", "", function( data )
			net.Start( "UpdateFactionPassword" )
				net.WriteString( data )
			net.SendToServer()
		end ):SetSkin( "flat" )
	end

	if LocalPlayer():Faction() then self:ViewFaction( LocalPlayer():Faction() ) end
end

function PANEL:ViewFaction( n )
	self.FactionName:SetText( basewars.GetFactionName( n ) or "Unassigned" )
	self.FactionName:SetColor( basewars.GetFactionColor( n ) or color_black )

	self.FactionMembers:Clear()
	for _, pl in ipairs( basewars.GetFactionPlayers( n ) ) do
		local line = self.FactionMembers:AddLine( nil, pl:Name(), pl:Frags(), pl:Deaths() )

		line.Player = pl

		local icon = vgui.Create( "DImage" )
		icon:SetSize( 16, 16 )
		icon:SetImage( basewars.GetFactionFounder( n ) == pl and "icon16/shield.png" or
			"icon16/user.png" )

		line:SetColumnText( 1, icon )
	end

	if LocalPlayer():Faction() == n then
		if basewars.GetFactionFounder( n ) == LocalPlayer() then
			self.Join:SetText( "Disband Faction" )
			self.Join:SetImage( "icon16/delete.png" )
		else
			self.Join:SetText( "Leave Faction" )
			self.Join:SetImage( "icon16/group_delete.png" )
		end

		function self.Join.DoClick()
			self:Close()

			net.Start( "LeaveFaction" )
			net.SendToServer()
		end
	else
		self.Join:SetText( "Join Faction" )
		self.Join:SetImage( "icon16/group_go.png" )

		function self.Join.DoClick()
			self:Close()

			if basewars.GetFactionPassword() then
				Derma_StringRequest( "Password required", "A password is required to join this faction.", "", function( data )
					net.Start( "JoinFaction" )
						net.WriteUInt( n, 16 )
						net.WriteString( data )
					net.SendToServer()
				end ):SetSkin( "flat" )
			else
				net.Start( "JoinFaction" )
					net.WriteUInt( n, 16 )
					net.WriteString( "" )
				net.SendToServer()
			end
		end
	end

	self.Join:SetEnabled( true )
	self.UpdatePassword:SetEnabled( basewars.GetFactionFounder( n ) == LocalPlayer() )
end

vgui.Register( "BasewarsFactionsMenu", PANEL, "DFrame" )