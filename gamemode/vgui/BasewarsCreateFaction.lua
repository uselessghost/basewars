local PANEL = {}

function PANEL:Init()
	self:SetTitle( "Create Faction" )
	self:SetSkin( "flat" )

	self.Panel = vgui.Create( "DPanel", self )
	self.Panel:Dock( FILL )
	self.Panel:DockPadding( 5, 5, 5, 5 )

	self.NameLabel = vgui.Create( "DLabel", self.Panel )
	self.NameLabel:Dock( TOP )
	self.NameLabel:SetText( "Name" )
	self.NameLabel:SetDark( true )

	self.Name = vgui.Create( "DTextEntry", self.Panel )
	self.Name:Dock( TOP )
	self.Name:SetText( string.format( "%s's Faction", LocalPlayer():Name() ) )

	self.PasswordLabel = vgui.Create( "DLabel", self.Panel )
	self.PasswordLabel:Dock( TOP )
	self.PasswordLabel:SetText( "Password (leave empty for none)" )
	self.PasswordLabel:SetDark( true )

	self.Password = vgui.Create( "DTextEntry", self.Panel )
	self.Password:Dock( TOP )

	self.ColorLabel = vgui.Create( "DLabel", self.Panel )
	self.ColorLabel:Dock( TOP )
	self.ColorLabel:SetText( "Color" )
	self.ColorLabel:SetDark( true )

	self.Color = vgui.Create( "DColorMixer", self.Panel )
	self.Color:Dock( FILL )

	self.Create = vgui.Create( "DButton", self )
	self.Create:Dock( BOTTOM )
	self.Create:DockMargin( 0, 5, 0, 0 )
	self.Create:SetText( "Create Faction" )
	self.Create:SetImage( "icon16/group_add.png" )

	function self.Create.DoClick()
		self:Close()

		net.Start( "CreateFaction" )
			net.WriteString( self.Name:GetText() )
			local col = self.Color:GetColor()
			net.WriteColor( Color( col.r, col.g, col.b ) )
			net.WriteString( self.Password:GetText() )
		net.SendToServer()
	end
end

vgui.Register( "BasewarsCreateFaction", PANEL, "DFrame" )