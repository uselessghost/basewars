local PANEL = {}

function PANEL:Init()	
	local Categories = {}

	for k, v in pairs( basewars.GetBuyables() ) do
		Categories[ v.Category ] = Categories[ v.Category ] or {}
		Categories[ v.Category ][ k ] = v
	end

	for k, v in SortedPairs( Categories ) do
		local cat = self:Add( k )

		local layout = vgui.Create( "DIconLayout" )
		layout:SetSpaceX( 1 )
		layout:SetSpaceY( 1 )
		cat:SetContents( layout )

		for k, v in SortedPairsByMemberValue( v, "Price" ) do
			local item = layout:Add( "BasewarsStoreItem" )
			item:SetSize( 125, 125 )
			item:SetName( v.Name )
			item:SetPrice( v.Price )
			item:SetModel( v.Model )

			if v.Icons then
				for k, v in ipairs( v.Icons ) do
					local icon = vgui.Create( "DImage", item )
					icon:SetSize( 16, 16 )
					icon:SetPos( 5 + ( k - 1 ) * 21, 5 )
					icon:SetImage( v )
				end
			end

			function item:DoClick()
				RunConsoleCommand( "bw", "buy", k )
			end
		end
	end
end

vgui.Register( "BasewarsStore", PANEL, "DCategoryList" )