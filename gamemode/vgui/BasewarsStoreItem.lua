local PANEL = {}

AccessorFunc( PANEL, "Name", "Name", FORCE_STRING )
AccessorFunc( PANEL, "Price", "Price", FORCE_NUMBER )

function PANEL:Paint( w, h )
	self.BaseClass.Paint( self, w, h )

	surface.SetDrawColor( Color( 0, 0, 0, 200 ) )
	surface.DrawRect( 0, h - 16, w, 16 )

	draw.SimpleText( self:IsHovered() and "$" .. string.Comma( self:GetPrice() ) or self:GetName(),
		"DermaDefault", w / 2, h - 9, color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
end 

function PANEL:SetModel( model )
	self.BaseClass.SetModel( self, model )

	local min, max = self.Entity:GetRenderBounds()
	self:SetCamPos( Vector( 1, 1, 1 ) * min:Distance( max ) * 0.6 )
	self:SetLookAt( ( min + max ) / 2 )
end

vgui.Register( "BasewarsStoreItem", PANEL, "DModelPanel" )