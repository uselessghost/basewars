local Raids = {}

function basewars.GetRaids()
	return Raids
end

local meta = FindMetaTable( "Player" )

function meta:GetRaids()
	local raids = {}

	for k, v in ipairs( Raids ) do
		if v.Raider == self or v.Target == self then table.insert( raids, v ) end
	end

	return raids
end

function meta:GetStartedRaids()
	local raids = {}

	for k, v in ipairs( Raids ) do
		if v.Raider == self then table.insert( raids, v ) end
	end

	return raids
end

function meta:GetTargettedRaid()
	for k, v in ipairs( Raids ) do
		if v.Target == self then return v end
	end
end

function meta:IsRaiding( pl, only_direct )
	for k, v in ipairs( Raids ) do
		if v.Raider == self and v.Target == pl then return true end
	end

	if not only_direct then
		if self:Faction() then
			for k, v in ipairs( basewars.GetFactionPlayers( self:Faction() ) ) do
				for _, raid in ipairs( v:GetStartedRaids() ) do
					if raid.Target:Faction() and raid.Target:Faction() == pl:Faction() then return true end
				end
			end
		end

		if pl:Faction() then
			for k, v in ipairs( basewars.GetFactionPlayers( pl:Faction() ) ) do
				local raid = v:GetTargettedRaid()
				if raid and raid.Raider == self then return true end
			end
		end
	end

	return false
end

if SERVER then
	local RaidDuration = CreateConVar( "bw_raid_duration", 600, FCVAR_NOTIFY )
	local RaidCooldown = CreateConVar( "bw_raid_cooldown", 600, FCVAR_NOTIFY )
	local RaidMaximum = CreateConVar( "bw_raid_maximum", 5, FCVAR_NOTIFY )
	local IllegalRaids = CreateConVar( "bw_raid_illegalraids", 0, { FCVAR_NOTIFY, FCVAR_REPLICATED } )

	function meta:HasScanner()
		for k, v in ipairs( self:GetOwnedEntities() ) do
			if v:GetClass() == "bw_scanner" then return true end
		end

		return false
	end

	function meta:IsRaidable()
		for k, v in ipairs( self:GetOwnedEntities() ) do
			if v.Raidable then return true end
		end

		return false
	end

	function GM:PlayerCanRaid( pl, target )
		return true
	end

	function GM:ModifyRaidDuration( duration, pl, target )
		return duration
	end

	function GM:ModifyRaidCooldown( cooldown, pl, raider )
		return cooldown
	end

	function GM:RaidStart( pl, target, duration )
	end

	function GM:RaidStop( pl, target )
	end

	function GM:RaidDisconnect( pl ) 
		for k, v in ipairs( pl:GetRaids() ) do
			if v.Raider == pl then
				v.Target:Notify( string.format( "%s's raid against you is over because they disconnected.", pl:Name() ) )
				basewars.StopRaid( pl, v.Target )
			else
				v.Raider:Notify( string.format( "Your raid against %s is over because they disconnected.", pl:Name() ) )
				basewars.StopRaid( v.Raider, pl )
			end
		end
	end

	function GM:RaidEntityDestroyed( ent )
		local pl = ent:GetCreator()
		if not IsValid( pl ) then return end

		for k, v in ipairs( pl:GetOwnedEntities() ) do
			if v ~= ent and v.Raidable then return end
		end

		for _, raid in ipairs( pl:GetRaids() ) do
			if raid.Raider == pl then	
				raid.Target:Notify( string.format( "You successfully defended against %s's raid!",
					pl:Name() ), 0, 8, "garrysmod/save_load1.wav" )
				pl:NotifyError( string.format( "Your raid against %s failed.", raid.Target:Name() ) )

				basewars.StopRaid( pl, raid.Target )
			else
				raid.Raider:Notify( string.format( "Your raid against %s was succesful!", pl:Name() ),
					0, 8, "garrysmod/save_load1.wav" )
				pl:NotifyError( string.format( "You failed to defend against %s's raid.",
					raid.Raider:Name() ) )

				basewars.StopRaid( raid.Raider, pl )
			end
		end
	end

	function meta:Scan( pl )
		if not IsValid( pl ) then return end

		if self == pl then self:NotifyError( "You can't scan yourself." ) return end

		if #self:GetStartedRaids() >= RaidMaximum:GetInt() then
			self:NotifyError( "You have too many raids running to start another." ) return end

		if not self:HasScanner() then
			self:NotifyError( "You need a scanner to begin raids." ) return end

		if self:IsRaiding( pl ) or pl:IsRaiding( self ) then
			self:NotifyError( "You are already raiding or being raided by that player." ) return end

		if pl:GetTargettedRaid() then
			self:NotifyError( "That player is already being raided." ) return end

		if pl:InFaction( self:Faction() ) then
			self:NotifyError( "You can't raid someone in your own faction." ) return end

		if pl.RaidCooldown and pl.RaidCooldown > CurTime() then
			self:NotifyError( "That player was recently raided and is in raid cooldown." ) return end

		if not pl:IsRaidable() then
			self:NotifyError( "That player has no valuable entities to raid." ) return end

		if not gamemode.Call( "PlayerCanRaid", self, pl ) then return end

		self:Notify( string.format( "You have begun a raid against %s!", pl:Name() ), 1, 8,
			"ambient/alarms/klaxon1.wav" )

		pl:Notify( string.format( "You are being raided by %s!", self:Name() ), 1, 8,
			"ambient/alarms/klaxon1.wav" )

		local duration = gamemode.Call( "ModifyRaidDuration", RaidDuration:GetFloat(), self, pl )
		basewars.StartRaid( self, pl, duration )
	end

	util.AddNetworkString( "StartRaid" )
	function basewars.StartRaid( pl, target, duration )
		pl.RaidCooldown = 0

		local id = table.insert( Raids, {
			Raider = pl,
			Target = target,
			Duration = duration,
			EndTime = CurTime() + duration
		} )

		timer.Create( "Raid." .. pl:UserID() .. "." .. target:UserID(), duration, 1, function()
			pl:Notify( string.format( "Your raid against %s timed out.", target:Name() ) )
			target:Notify( string.format( "%s's raid against you timed out.", pl:Name() ) )

			basewars.StopRaid( pl, target )
		end )

		net.Start( "StartRaid" )
			net.WriteUInt( id, 16 )
			net.WriteEntity( pl )
			net.WriteEntity( target )
			net.WriteFloat( duration )
			net.WriteFloat( CurTime() + duration )
		net.Broadcast()

		gamemode.Call( "RaidStart", pl, target, duration )

		return id
	end

	util.AddNetworkString( "StopRaid" )
	function basewars.StopRaid( pl, target )
		for k, v in ipairs( Raids ) do
			if v.Raider == pl and v.Target == target then
				local cooldown = gamemode.Call( "ModifyRaidCooldown", RaidCooldown:GetFloat(),
					target, pl )
				target.RaidCooldown = CurTime() + cooldown

				table.remove( Raids, k )

				net.Start( "StopRaid" )
					net.WriteUInt( k, 16 )
				net.Broadcast()
			end
		end

		timer.Destroy( "Raid." .. pl:UserID() .. "." .. target:UserID() )

		gamemode.Call( "RaidStop", pl, Target )
	end

	function basewars.SyncRaids( pl )
		for k, v in ipairs( Raids ) do
			net.Start( "StartRaid" )
				net.WriteUInt( k, 16 )
				net.WriteEntity( v.Raider )
				net.WriteEntity( v.Target )
				net.WriteFloat( v.Duration )
				net.WriteFloat( v.EndTime )
			net.Send( pl )
		end
	end

	basewars.AddCommand( "scan", function( pl, args )
		if #args <= 0 then return pl:ConCommand( "bw_scan" ) end

		local target = basewars.FindPlayer( table.concat( args, " " ) )
		if not IsValid( target ) then pl:NotifyError( "Player not found." ) return end

		pl:Scan( target )
	end )

	basewars.AddCommand( "stopraid", function( pl, args )
		local target = basewars.FindPlayer( table.concat( args, " " ) )
		if not IsValid( target ) then pl:NotifyError( "Player not found." ) return end

		if not pl:IsRaiding( target, true ) then
			pl:NotifyError( "You're not raiding that player." ) return end

		basewars.StopRaid( pl, target )

		pl:Notify( string.format( "You stopped your raid against %.", target:Name() ) )
		target:Notify( string.format( "%s stopped their raid against you.", pl:Name() ) )
	end )
else
	net.Receive( "StartRaid", function()
		Raids[ net.ReadUInt( 16 ) ] = {
			Raider = net.ReadEntity(),
			Target = net.ReadEntity(),
			Duration = net.ReadFloat(),
			EndTime = net.ReadFloat()
		}
	end )

	net.Receive( "StopRaid", function()
		table.remove( Raids, net.ReadUInt( 16 ) )
	end )

	function GM:DrawRaidIndicator( x, y, w, h, raider, target, duration, endtime )
		local time = endtime - CurTime()
		local m = math.floor( time / 60 )
		local s = math.floor( time % 60 )

		self:DrawRect( x, y, w, h )

		draw.SimpleText( raider:Name(), "DermaDefault", x + 5, y + 5, Color( 50, 255, 50 ) )
		draw.SimpleText( target:Name(), "DermaDefault", x + w - 5, y + 5, Color( 255, 50, 50 ),
			TEXT_ALIGN_RIGHT )

		local mul = time / duration

		local col = Color( 0, 0, 0 )
		col.r = Lerp( mul, 255, 50 )
		col.g = Lerp( mul, 50, 255 )
		col.b = 50

		self:DrawProgressBar( x + 5, y + 24, w- 10, h - 24 - 5, mul, col,
			string.format( "%d:%02d", m, s ), "DermaDefaultBold", true )
	end

	function GM:DrawRaids()
		for k, v in ipairs( Raids ) do
			if IsValid( v.Raider ) and IsValid( v.Target ) then
				local w, h = 200, 48
				local x, y = 5, 5 + ( k - 1 ) * ( h + 1 )

				self:DrawRaidIndicator( x, y, w, h, v.Raider, v.Target, v.Duration, v.EndTime )
			else
				Raids[ k ] = nil
			end
		end
	end

	concommand.Add( "bw_scan", function()
		local p = vgui.Create( "BasewarsScanMenu" )
		p:SetSize( 250, 400 )
		p:Center()
		p:MakePopup()
	end )
end