EFFECT.Beam = Material( "trails/laser" )
EFFECT.Duration = 3

function EFFECT:Init( data )
	self.DieTime = CurTime() + self.Duration

	self.Start = data:GetStart()
	self.End = data:GetOrigin()
	self:SetRenderBoundsWS( self.Start, self.End )

	self.Emitter = ParticleEmitter( vector_origin )

	for i = 1, self.Start:Distance( self.End ), 8 do
		local p = self.Emitter:Add( "sprites/glow04_noz", self.Start + ( self.End - self.Start ) * ( i / self.Start:Distance( self.End ) ) )
		p:SetLifeTime( 0 )
		p:SetDieTime( self.Duration + math.random() )
		p:SetStartSize( 64 )
		p:SetEndSize( 0 )
		p:SetStartAlpha( 255 )
		p:SetEndAlpha( 0 )
		p:SetColor( 100, 200, 255 )
		p:SetGravity( VectorRand() * 4 )
	end

	for i = 1, 32 do
		local p = self.Emitter:Add( "sprites/glow04_noz", self.End )
		p:SetLifeTime( 0 )
		p:SetDieTime( self.Duration + math.random() )
		p:SetStartSize( 2048 )
		p:SetEndSize( 4096 )
		p:SetStartAlpha( 255 )
		p:SetEndAlpha( 0 )
		p:SetColor( 100, 200, 255 )
		p:SetVelocity( VectorRand() * 1024 )
		p:SetAirResistance( 128 )
	end

	for i = 1, 16 do
		local p = self.Emitter:Add( "sprites/glow04_noz", self.End )
		p:SetLifeTime( 0 )
		p:SetDieTime( 2 )
		p:SetStartSize( 64 )
		p:SetEndSize( 64 )
		p:SetStartLength( 2048 )
		p:SetEndLength( 4096 )
		p:SetStartAlpha( 255 )
		p:SetEndAlpha( 0 )
		p:SetColor( 100, 200, 255 )
		p:SetVelocity( VectorRand() * 16 )
	end

	local p = self.Emitter:Add( "particle/warp1_warp", self.End )
	p:SetLifeTime( 0 )
	p:SetDieTime( 4 )
	p:SetStartSize( 0 )
	p:SetEndSize( 4096 )
	p:SetStartAlpha( 255 )
	p:SetEndAlpha( 0 )
	p:SetColor( 255, 255, 255 )

	self.Emitter:Finish()

	util.ScreenShake( self.End, 25, 10, 1, 512 )
	sound.Play( "ambient/explosions/explode_2.wav", self.End, 150, 100, 1  )
end

function EFFECT:Think()
	return CurTime() < self.DieTime
end

function EFFECT:Render()
	local mul = ( self.DieTime - CurTime() ) / self.Duration

	render.SetMaterial( self.Beam )
	render.DrawBeam( self.Start, self.End, mul * 256, 0, 64, Color( 100, 200, 255, mul * 255 ) )
end