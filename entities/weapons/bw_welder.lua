AddCSLuaFile()

SWEP.PrintName = "Welder"
SWEP.Category = "BaseWars"
SWEP.Instructions = [[Primary fire: Repair structures
Secondary fire: Destroy structures]]

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.HoldType = "ar2"

SWEP.WorldModel = Model( "models/weapons/w_irifle.mdl" )
SWEP.ViewModel = Model( "models/weapons/c_irifle.mdl" )
SWEP.ViewModelFOV = 55
SWEP.UseHands = true

SWEP.Slot = 5
SWEP.SlotPos = 1

SWEP.Primary.Ammo = "none"
SWEP.Primary.Automatic = true
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Automatic = true
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1

function SWEP:Initialize()
	self:SetHoldType( self.HoldType )
end

function SWEP:Repair( amount )
	if self:GetNextPrimaryFire() > CurTime() then return end

	self:SetNextPrimaryFire( CurTime() + 0.1 )

	self:ShootEffects()
	self:EmitSound( "ambient/energy/spark" .. math.random( 1, 6 ) .. ".wav" )

	if CLIENT then return end

	local tr = util.TraceLine{
		start = self.Owner:EyePos(),
		endpos = self.Owner:EyePos() + self.Owner:GetAimVector() * 64,
		filter = self.Owner
	}

	if not IsValid( tr.Entity ) or not tr.Entity.Destructable then return end

	local ef = EffectData()
	ef:SetOrigin( tr.HitPos )
	ef:SetNormal( tr.HitNormal )
	ef:SetMagnitude( 2 )
	ef:SetRadius( 8 )
	util.Effect( "Sparks", ef, true, true )

	tr.Entity:SetDurability( math.min( tr.Entity:GetDurability() + amount, tr.Entity:GetMaxDurability() ) )

	if tr.Entity:GetDurability() <= 0 then
		tr.Entity:Destroy()
		tr.Entity:Remove()
	end
end

function SWEP:PrimaryAttack()
	self:Repair( 5 )
end

function SWEP:SecondaryAttack()
	self:Repair( -1 )
end