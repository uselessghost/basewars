AddCSLuaFile()

SWEP.PrintName = "Omega Laser"
SWEP.Category = "BaseWars"
SWEP.Instructions = "Hold Primary Fire: Charge laser"

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.HoldType = "rpg"

SWEP.WorldModel = Model( "models/weapons/w_rocket_launcher.mdl" )
SWEP.ViewModel = Model( "models/weapons/c_rpg.mdl" )
SWEP.ViewModelFOV = 55
SWEP.UseHands = true

SWEP.Slot = 4
SWEP.SlotPos = 1

SWEP.ChargeDuration = 10

SWEP.Damage = 1000
SWEP.Radius = 1024

SWEP.Primary.Ammo = "none"
SWEP.Primary.Automatic = false
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Ammo = "none"
SWEP.Secondary.Automatic = false
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1

function SWEP:Initialize()
	self:SetHoldType( self.HoldType )

	hook.Add( "SetupMove", self, self.SlowPlayer )
end

function SWEP:SetupDataTables()
	self:NetworkVar( "Float", 0, "ChargeTime" )
end

function SWEP:SlowPlayer( pl, mv, cmd )
	if pl:GetActiveWeapon() == self and self:IsCharging() then
		mv:SetMaxClientSpeed( pl:GetWalkSpeed() * 0.5 )
	end
end

function SWEP:IsCharging()
	return self:GetChargeTime() ~= 0
end

function SWEP:ChargeLaser()
	self:EmitSound( "WeaponDissolve.Beam" )
	self:SetChargeTime( CurTime() + self.ChargeDuration )
end

function SWEP:FireLaser()
	self:StopSound( "WeaponDissolve.Beam" )

	local tr = util.TraceLine{
		start = self.Owner:EyePos(),
		endpos = self.Owner:EyePos() + self.Owner:GetAimVector() * 2 ^ 16,
		filter = self.Owner
	}

	self:ShootEffects()
	self:EmitSound( "beams/beamstart5.wav" )

	if CLIENT then return end
	
	local ef = EffectData()
	ef:SetStart( tr.StartPos )
	ef:SetOrigin( tr.HitPos )
	util.Effect( "OmegaLaser", ef, true, true )

	self.Owner:SetVelocity( self.Owner:GetAimVector() * -1024 )

	local dmg = DamageInfo()
	dmg:SetAttacker( self.Owner )
	dmg:SetInflictor( self.Owner )
	dmg:SetDamage( self.Damage )
	dmg:SetDamageType( DMG_BLAST + DMG_DISSOLVE )
	dmg:SetDamagePosition( tr.HitPos )

	util.BlastDamageInfo( dmg, tr.HitPos, self.Radius )
end

if CLIENT then SWEP.Emitter = ParticleEmitter( vector_origin ) end
function SWEP:Think()
	local mul = 1 - ( self:GetChargeTime() - CurTime() ) / self.ChargeDuration

	if self:IsCharging() then
		util.ScreenShake( self.Owner:GetPos(), mul * 5, 10, 0.5, 100 )

		if self:GetChargeTime() < CurTime() then
			self:FireLaser()
			self:SetChargeTime( 0 )
		end

		if not self.Owner:KeyDown( IN_ATTACK ) then
			self:SetChargeTime( 0 )
			self:StopSound( "WeaponDissolve.Beam" )
		end
	end

	if CLIENT then
		if self:IsCharging() then
			if ( self:GetChargeTime() - CurTime() ) > 1 then
				local pos = self:GetAttachment( 1 ).Pos

				if not self.Owner:ShouldDrawLocalPlayer() then
					pos = self.Owner:GetViewModel():GetAttachment( 1 ).Pos
				end

				local p = self.Emitter:Add( "sprites/glow04_noz", pos )
				p:SetLifeTime( 0 )
				p:SetDieTime( 1 )
				p:SetStartSize( 0 )
				p:SetEndSize( 64 * mul )
				p:SetStartAlpha( 255 )
				p:SetEndAlpha( 0 )
				p:SetColor( 100, 200, 255 )
				p:SetVelocity( VectorRand() * 8 )

				local p = self.Emitter:Add( "sprites/glow04_noz", pos + VectorRand() )
				p:SetLifeTime( 0 )
				p:SetDieTime( 1 )
				p:SetStartLength( 128 * mul )
				p:SetEndLength( 4 * mul )
				p:SetStartSize( 32 * mul )
				p:SetEndSize( 0 )
				p:SetStartAlpha( 0 )
				p:SetEndAlpha( 255 )
				p:SetColor( 100, 200, 255 )
				p:SetVelocity( p:GetPos() - pos )
			end
		end
	end
end

function SWEP:PrimaryAttack()
	if not self:IsCharging() then self:ChargeLaser() end
end

function SWEP:SecondaryAttack()
end