AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_powered"
ENT.InfoIcon = "icon16/shield.png"

ENT.PrintName = "Armor Regenerator"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 250

ENT.EnergyCost = 100

if SERVER then
	function ENT:Initialize()
		self:SetModel( "models/props_combine/suit_charger001.mdl" )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self.MaxDurability )
	end

	function ENT:Use( pl )
		if self:HasEnergy( self.EnergyCost ) then
			pl:SetArmor( 100 )

			self:ConsumeEnergy( self.EnergyCost )
			self:EmitSound( "ItemBattery.Touch" )
		else
			self:EmitSound( "buttons/button18.wav" )
		end
	end
end