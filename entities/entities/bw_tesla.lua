AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_turret"
ENT.PrintName = "Tesla Coil"
ENT.InfoIcon = "icon16/lightning.png"

ENT.Model = Model( "models/props_c17/utilityconnecter006c.mdl" )

ENT.EnergyCost = 100
ENT.Range = 512
ENT.Speed = 0.75
ENT.Upgrades = {
	{ Damage = 20 },
	{ Cost = 25000, Damage = 30 },
	{ Cost = 50000, Damage = 40 }
}
ENT.FireSound = Sound( "NPC_RollerMine.Shock" )
ENT.MaxDurability = 100
ENT.Raidable = true

if SERVER then
	function ENT:FindTargets()
		local pos = self:MuzzlePosition()
		local enemies = {}

		for k, v in ipairs( ents.FindInSphere( pos, self.Range ) ) do
			if IsValid( v ) and v:IsPlayer() and self:CanHit( v ) and self:ShouldTarget( v ) then
				table.insert( enemies, v )
			end
		end

		return enemies
	end
	
	function ENT:Think()
		local targets = self:FindTargets()

		for k, v in ipairs( targets ) do
		 	self:Tesla( v )
		end

		self:NextThink( CurTime() + self.Speed )
		return true
	end

	function ENT:MuzzlePosition()
		return self:LocalToWorld( self:OBBCenter() ) + self:GetAngles():Up() * 25
	end

	function ENT:Tesla( pl )
		if not self:HasEnergy( self.EnergyCost ) then return end

		self:EmitSound( self.FireSound )

		local dmg = DamageInfo()
		dmg:SetDamage( self.Upgrades[ self:GetLevel() ].Damage )
		dmg:SetDamageType( DMG_SHOCK + DMG_DISSOLVE )
		dmg:SetDamagePosition( self:MuzzlePosition() )
		dmg:SetAttacker( self:GetCreator() )
		dmg:SetInflictor( self )

		local ef = EffectData()
		ef:SetEntity( pl )
		ef:SetStart( self:MuzzlePosition() )
		ef:SetOrigin( pl:LocalToWorld( pl:OBBCenter() ) )
		ef:SetScale( 4 )
		ef:SetAttachment( 1 )
		util.Effect( "ToolTracer", ef, true, true )
		util.Effect( "TeslaHitboxes", ef, true, true )

		pl:TakeDamageInfo( dmg )

		self:ConsumeEnergy( self.EnergyCost )
	end
end