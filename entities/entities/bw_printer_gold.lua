AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_printer"

ENT.PrintName = "Gold Printer"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 200

ENT.EnergyCost = 100
ENT.PrintInterval = 40
ENT.MaxPaper = 60
ENT.Upgrades = {
	{ Output = 400 },
	{ Cost = 38443, Output = 800 },
	{ Cost = 57665, Output = 1600 },
	{ Cost = 86498, Output = 3200 },
	{ Cost = 129746, Output = 6400 }
}

ENT.Color = Color( 235, 180, 80 )

ENT.Raidable = true