AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_powered"
ENT.InfoIcon = "icon16/printer.png"

ENT.MaxDurability = 1

ENT.EnergyCost = 1
ENT.PrintInterval = 1
ENT.MaxPaper = 50

ENT.Upgrades = {
	{ Output = 1 },
	{ Cost = 10, Output = 2 },
	{ Cost = 20, Output = 3 },
	{ Cost = 30, Output = 4 },
	{ Cost = 40, Output = 5 }
}

ENT.Model = Model( "models/props_lab/reciever01a.mdl" )
ENT.Color = color_white

ENT.Printer = true
ENT.Upgradable = true

function ENT:SetupDataTables()
	self:SetupDurability( 0, 1 )
	self:NetworkVar( "Int", 2, "StoredMoney" )
	self:NetworkVar( "Int", 3, "Paper" )
	self:NetworkVar( "Int", 4, "Level" )
	self:NetworkVar( "Float", 0, "NextPrint" )
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( self.Model )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self.MaxDurability )
		self:SetPaper( self.MaxPaper )
		self:SetLevel( 1 )
		self:SetNextPrint( CurTime() + self.PrintInterval )

		self:SetColor( self.Color )
		self:SetMaterial( "models/shiny" )
	end

	function ENT:Think()
		if CurTime() > self:GetNextPrint() then
			self:PrintMoney()
			self:SetNextPrint( CurTime() + self.PrintInterval )
		end
	end

	function ENT:GetNearestMoneyVault()
		for k, v in ipairs( ents.FindInSphere( self:GetPos(), 512 ) ) do
			if v:GetClass() == "bw_moneyvault" and self:GetCreator() == v:GetCreator() then
				return v end
		end
	end

	function ENT:CanPrint()
		return self:GetPaper() > 0 and self:HasEnergy( self.EnergyCost )
	end

	function ENT:PrintMoney()
		if self:CanPrint() then
			local money = self.Upgrades[ self:GetLevel() ].Output
			local vault = self:GetNearestMoneyVault()

			if IsValid( vault ) then
				vault:SetStoredMoney( vault:GetStoredMoney() + money )
			else
				self:SetStoredMoney( self:GetStoredMoney() + money )
			end

			self:SetPaper( self:GetPaper() - 1 )
			self:ConsumeEnergy( self.EnergyCost )
			self:EmitSound( "buttons/button6.wav" )
		else
			self:EmitSound( "buttons/button18.wav" )
		end
	end

	function ENT:Use( pl )
		pl:GiveMoney( self:GetStoredMoney() )
		pl:Notify( string.format( "You gathered $%s from the printer.", string.Comma( self:GetStoredMoney() ) ) )

		self:SetStoredMoney( 0 )
	end

	function ENT:OnDestroyed()
		if self:GetStoredMoney() > 0 then
			local ent = ents.Create( "bw_money" )
			ent:SetPos( self:LocalToWorld( self:OBBCenter() ) )
			ent:SetAmount( self:GetStoredMoney() )
			ent:Spawn()
		end
	end
else
	function ENT:GetPopover()
		local s = math.floor( self:GetNextPrint() - CurTime() )
		local m = math.floor( s / 60 )
		s = s - m * 60

		return {
			self:DurabilityGauge(),
			self:LevelGauge(),
			{
				Type = "gauge",
				Icon = "icon16/page_white.png",
				Text = string.format( "Paper: %s/%s", self:GetPaper(), self.MaxPaper ),
				Value = self:GetPaper(),
				Maximum = self.MaxPaper
			},
			{
				Type = "gauge",
				Icon = "icon16/clock.png",
				Text = string.format( "Next print in %d:%02d", m, s ),
				Value = self.PrintInterval - ( self:GetNextPrint() - CurTime() ),
				Maximum = self.PrintInterval
			},
			{
				Type = "text",
				Icon = "icon16/money.png",
				Text = string.format( "Stored money: $%s", string.Comma( self:GetStoredMoney() ) )
			}
		}
	end
end