AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_ammo"

ENT.PrintName = "Grenade Ammo"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/Items/grenadeAmmo.mdl" )

ENT.AmmoType = "grenade"
ENT.Amount = 6