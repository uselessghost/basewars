AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_ammo"

ENT.PrintName = "Rocket Ammo"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/weapons/w_missile_closed.mdl" )

ENT.AmmoType = "rpg_round"
ENT.Amount = 12