AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"

ENT.PrintName = "Spawnpoint"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

if SERVER then
	function ENT:SpawnFunction( pl, tr, class )
		local ent = ents.Create( class )
		ent:SetPos( pl:GetPos() )
		ent:SetPlayer( pl )
		ent:Spawn()

		return ent
	end

	function ENT:Initialize()
		self:SetModel( "models/props_combine/combine_mine01.mdl" )
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_NONE )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		self:SetMaxDurability( 200 )
		self:SetDurability( self:GetMaxDurability() )

		hook.Add( "PlayerSelectSpawn", self, self.SelectSpawn )
	end

	function ENT:SelectSpawn( pl )
		if self:GetCreator() == pl then return self end
	end
end