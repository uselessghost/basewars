AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"
ENT.PrintName = "Printer Paper"
ENT.InfoIcon = "icon16/page_white.png"

ENT.MaxDurability = 10

if SERVER then
	function ENT:Initialize()
		self:SetModel( "models/props_junk/garbage_newspaper001a.mdl" )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self:GetMaxDurability() )
	end

	function ENT:PhysicsCollide( col )
		if self.Used then return end

		local ent = col.HitEntity
		if not IsValid( ent ) or not ent.Printer then return end

		self:RefuelPrinter( ent )
		self:Remove()

		self.Used = true
	end

	function ENT:RefuelPrinter( ent )
		ent:EmitSound( "buttons/latchlocked2.wav" )
		ent:SetPaper( ent.MaxPaper )
	end
end