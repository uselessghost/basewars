AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_powered"
--ENT.InfoIcon = "icon16/gun.png" 

ENT.Upgrades = {
	{}
}

ENT.Model = Model( "models/combine_turrets/floor_turret.mdl" )

ENT.MaxDurability = 100

function ENT:SetupDataTables()
	self:SetupDurability( 0, 1 )
	self:NetworkVar( "Int", 2, "Level" )	
	self:NetworkVar( "String", 0, "CurrentProduct" )	
	self:NetworkVar( "Float", 0, "ManufacturingTime" )
end

function ENT:IsManufacturing()
	return self:GetCurrentProduct() > 0 and self:GetManufacturingTime() > CurTime()
end

function ENT:GetProducts()
	return self.Products or {}
end

function ENT:AddProduct( opts )
	if not self.Products then self.Products = {} end

	if not opts.Class or not opts.Name or not opts.Cost or not opts.Time then return end

	--opts.

	self.Products[ class ] = opts
end

function ENT:RemoveProduct( class )
	if self.Products then self.Products[ class ] = nil end
end

function ENT:AddProducts()
	-- override
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( self.Model )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self.MaxDurability )
		self:SetLevel( 1 )

		self:AddProducts()
	end

	function ENT:Think()
	end
else
	function ENT:Initialize()
		self:AddProducts()
	end

	function ENT:GetPopover()
		return {
			self:DurabilityGauge(),
			self:LevelGauge(),

		}
	end
end