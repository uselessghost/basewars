AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_printer"

ENT.PrintName = "Bronze Printer"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 100

ENT.EnergyCost = 30
ENT.PrintInterval = 60
ENT.MaxPaper = 50
ENT.Upgrades = {
	{ Output = 100 },
	{ Cost = 1500, Output = 200 },
	{ Cost = 2250, Output = 400 },
	{ Cost = 3380, Output = 800 },
	{ Cost = 5063, Output = 1600 }
}

ENT.Color = Color( 140, 50, 0 )