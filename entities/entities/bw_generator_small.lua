AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_generator"

ENT.PrintName = "Small Generator"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/props_c17/trappropeller_engine.mdl" )

ENT.MaxDurability = 300

ENT.MaxEnergy = 500
ENT.EnergyPerSecond = 1