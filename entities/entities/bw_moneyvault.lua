AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"
ENT.InfoIcon = "icon16/money_dollar.png"

ENT.PrintName = "Money Vault"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 500

function ENT:SetupDataTables()
	self:SetupDurability( 0, 1 )
	self:NetworkVar( "Int", 2, "StoredMoney" )
end

if SERVER then
	function ENT:SpawnFunction( pl, tr, class )
		local ent = ents.Create( class )
		ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
		ent:Spawn()

		return ent
	end

	function ENT:Initialize()
		self:SetModel( "models/props_wasteland/kitchen_fridge001a.mdl" )
		
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self:GetMaxDurability() )
	end	

	function ENT:Use( pl )
		pl:GiveMoney( self:GetStoredMoney() )
		pl:Notify( string.format( "You gathered $%s from the money vault.", string.Comma( self:GetStoredMoney() ) ) )

		self:SetStoredMoney( 0 )
	end

	function ENT:OnDestroyed()
		if self:GetStoredMoney() > 0 then
			local ent = ents.Create( "bw_money" )
			ent:SetPos( self:LocalToWorld( self:OBBCenter() ) )
			ent:SetAmount( self:GetStoredMoney() )
			ent:Spawn()
		end
	end
else
	function ENT:GetPopover()
		return {
			self:DurabilityGauge(),
			{
				Type = "text",
				Text = string.format( "Stored money: $%s", string.Comma( self:GetStoredMoney() ) ),
				Icon = "icon16/money.png"
			}
		}
	end
end