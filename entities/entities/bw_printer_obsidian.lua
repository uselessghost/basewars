AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_printer"

ENT.PrintName = "Obsidian Printer"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 300

ENT.EnergyCost = 500
ENT.PrintInterval = 15
ENT.MaxPaper = 70
ENT.Upgrades = {
	{ Output = 1600 },
	{ Cost = 985261, Output = 3200 },
	{ Cost = 1477892, Output = 6400 },
	{ Cost = 2216838, Output = 12800 },
	{ Cost = 3325257, Output = 25600 }
}

ENT.Model = Model( "models/props_c17/consolebox01a.mdl" )
ENT.Color = Color( 75, 25, 100 )

ENT.Raidable = true