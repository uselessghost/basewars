AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_ammo"

ENT.PrintName = "Pistol Ammo"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/Items/BoxSRounds.mdl" )

ENT.AmmoType = "pistol"
ENT.Amount = 30