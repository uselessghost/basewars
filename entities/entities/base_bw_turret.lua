AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_powered"
ENT.InfoIcon = "icon16/gun.png" 

ENT.Model = Model( "models/combine_turrets/floor_turret.mdl" )

ENT.EnergyCost = 1

ENT.Spread = 0.05
ENT.Range = 512
ENT.Speed = 0.25

ENT.Upgradable = true
ENT.Upgrades = {
	{ Damage = 1 },
	{ Cost = 1, Damage = 2 },
	{ Cost = 2, Damage = 3 }
}

ENT.Tracer = "Tracer"
ENT.MuzzleEffect = "MuzzleEffect"

ENT.FireSound = Sound( "NPC_FloorTurret.Shoot" )

ENT.MaxDurability = 100

function ENT:SetupDataTables()
	self:SetupDurability( 0, 1 )
	self:NetworkVar( "Int", 2, "Level" )
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( self.Model )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self.MaxDurability )

		self:SetLevel( 1 )
	end

	function ENT:Think()
		local target = self:FindNearestTarget()

		if IsValid( target ) then
			self:Shoot( self:MuzzlePosition(), target:LocalToWorld( target:OBBCenter() ) )
		end

		self:NextThink( CurTime() + self.Speed )
		return true
	end

	function ENT:MuzzlePosition()
		return self:GetAttachment( 1 ).Pos, self:GetAttachment( 1 ).Ang
	end

	function ENT:ShootEffects()
		local pos, ang = self:MuzzlePosition()

		local ef = EffectData()
		ef:SetOrigin( pos )
		ef:SetAngles( ang )
		util.Effect( self.MuzzleEffect, ef, true, true )
	end

	function ENT:CanHit( pl )
		return not util.TraceLine{
			start = self:MuzzlePosition(),
			endpos = pl:LocalToWorld( pl:OBBCenter() ),
			filter = { self, pl }
		}.Hit
	end

	function ENT:ShouldTarget( pl )
		return pl:Alive() and IsValid( self:GetCreator() ) and self:GetCreator():IsEnemy( pl )
	end

	function ENT:FindTargets()
		local pos = self:MuzzlePosition()
		local dir = self:GetAngles():Forward()

		local enemies = {}

		for k, v in ipairs( ents.FindInCone( pos, dir, self.Range, 75 ) ) do
			if IsValid( v ) and v:IsPlayer() and self:CanHit( v ) and self:ShouldTarget( v ) then
				table.insert( enemies, v )
			end
		end

		return enemies
	end

	function ENT:FindNearestTarget()
		local enemies = self:FindTargets()
		local nearest

		for k, v in ipairs( enemies ) do
			local our_dist = v:GetPos():Distance( self:MuzzlePosition() )
			local their_dist = nearest and nearest:GetPos():Distance( self:MuzzlePosition() )

			if not nearest or our_dist < their_dist then nearest = v end
		end

		return nearest
	end

	function ENT:CalculateDirection( at )
		local pos = self:MuzzlePosition()
		local dir = ( at - pos ):GetNormal()

		return dir
	end

	function ENT:PoseMuzzle( dir )
		local dir = ( dir * 1 )
		dir:Rotate( -self:GetAngles() )

		local ang = dir:GetNormal():Angle()
		ang:Normalize()

		self:SetPoseParameter( "aim_pitch", ang.p )
		self:SetPoseParameter( "aim_yaw", ang.y )

		--self:InvalidateBoneCache()
	end

	function ENT:Shoot( pos, at )
		if not self:HasEnergy( self.EnergyCost ) then return end

		self:EmitSound( self.FireSound )

		local dir = self:CalculateDirection( at )

		self:ShootEffects()
		self:PoseMuzzle( dir )

		self:FireBullets{
			Src = pos,
			Dir = dir,
			Attacker = self:GetCreator(),
			Damage = self.Upgrades[ self:GetLevel() ].Damage,
			Spread = Vector( self.Spread, self.Spread, 0 ),
			TracerName = self.Tracer
		}

		self:ConsumeEnergy( self.EnergyCost )
	end
else
	function ENT:GetPopover()
		return {
			self:DurabilityGauge(),
			self:LevelGauge()
		}
	end
end