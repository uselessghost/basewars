AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_consumable"
ENT.InfoIcon = "icon16/heart.png"

if CLIENT then return end

function ENT:Consume( pl )
	--if pl:Health() >= pl:GetMaxHealth() then return end
	
	pl:SetHealth( math.Clamp( pl:Health() + self.HealingAmount, 0, pl:GetMaxHealth() ) )
	pl:EmitSound( "HealthKit.Touch" )
end