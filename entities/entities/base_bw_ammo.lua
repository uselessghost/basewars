AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_consumable"
ENT.InfoIcon = "icon16/gun.png"

ENT.AmmoType = "pistol"
ENT.Amount = 10

if CLIENT then return end

function ENT:Consume( pl )
	pl:GiveAmmo( self.Amount, self.AmmoType )
	pl:EmitSound( "Player.PickupWeapon" )
end