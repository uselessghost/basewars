AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_generator"

ENT.PrintName = "Nuclear Generator"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/props_wasteland/laundry_washer003.mdl" )

ENT.MaxDurability = 1000

ENT.MaxEnergy = 50000
ENT.EnergyPerSecond = 100

ENT.Raidable = true