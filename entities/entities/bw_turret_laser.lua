AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_turret"
ENT.PrintName = "Laser Turret"

ENT.EnergyCost = 25

ENT.Spread = 0.10
ENT.Range = 1024
ENT.Speed = 0.1
ENT.Upgrades = {
	{ Damage = 12 },
	{ Cost = 10000, Damage = 14 },
	{ Cost = 25000, Damage = 16 }
}

ENT.Tracer = "LaserTracer"
ENT.MuzzleEffect = "MuzzleEffect"

ENT.FireSound = Sound( "Weapon_AR2.Single" )

ENT.MaxDurability = 100

ENT.Raidable = true