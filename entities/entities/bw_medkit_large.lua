AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_medkit"

ENT.PrintName = "Large Medkit"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/Items/HealthKit.mdl" )
ENT.HealingAmount = 100