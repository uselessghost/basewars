AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_powered"
ENT.InfoIcon = "icon16/gun.png"

ENT.PrintName = "Ammo Manufacturer"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 500

ENT.EnergyCost = 100
ENT.AmmoAmount = 30

if SERVER then
	function ENT:Initialize()
		self:SetModel( "models/props_lab/reciever_cart.mdl" )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self.MaxDurability )
	end

	function ENT:Use( pl )
		if self:HasEnergy( self.EnergyCost ) then
			pl:GiveAmmo( self.AmmoAmount, pl:GetActiveWeapon():GetPrimaryAmmoType() )

			self:ConsumeEnergy( self.EnergyCost )
			self:EmitSound( "buttons/button6.wav" )
		else
			self:EmitSound( "buttons/button18.wav" )
		end
	end
end