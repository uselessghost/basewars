AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_consumable"
ENT.InfoIcon = "icon16/shield.png"

if CLIENT then return end

function ENT:Consume( pl )
	--if pl:Armor() >= 100 then return end
	
	pl:SetArmor( math.Clamp( pl:Armor() + self.ArmorAmount, 0, 100 ) )
	pl:EmitSound( "ItemBattery.Touch" )
end