AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_consumable"
ENT.InfoIcon = "icon16/gun.png"

ENT.PrintName = "Weapon"

if SERVER then
	AccessorFunc( ENT, "WeaponClass", "WeaponClass", FORCE_STRING )

	function ENT:Consume( pl )
		if pl:HasWeapon( self:GetWeaponClass() ) then return false end

		pl:EmitSound( "Item.Pickup" )
		pl:Give( self:GetWeaponClass() )
		pl:SelectWeapon( self:GetWeaponClass() )
	end
end