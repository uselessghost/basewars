AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_consumable"
ENT.InfoIcon = "icon16/money.png"

ENT.PrintName = "Money"

ENT.Model = Model( "models/props/cs_assault/money.mdl" )

function ENT:SetupDataTables()
	self:NetworkVar( "Int", 0, "Amount" )
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( self.Model )
		
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()
	end

	function ENT:Consume( pl )
		pl:GiveMoney( self:GetAmount() )
		pl:Notify( "You found $" .. string.Comma( self:GetAmount() ) .. "." )
	end

	function ENT:EntityTakeDamage() end
else
	function ENT:Draw()
		self:DrawModel()

		local pos = self:GetPos() + self:GetAngles():Up() * 0.87
		local ang = self:GetAngles()
		local scale = 0.05

		cam.Start3D2D( pos, ang, scale )
			draw.SimpleTextOutlined( "$" .. string.Comma( self:GetAmount() ), "DermaLarge", 0, 0,
				color_white, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, color_black )
		cam.End3D2D()
	end

	function ENT:GetPopover()
		return {}
	end
end
