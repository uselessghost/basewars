AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"
ENT.InfoIcon = "icon16/lightbulb.png"

ENT.Generator = true

ENT.MaxDurability = 1

ENT.MaxFuel = 900
ENT.MaxEnergy = 1000
ENT.EnergyPerSecond = 1

function ENT:SetupEnergy( energy_id, max_energy_id, fuel_id )
	self:NetworkVar( "Int", energy_id, "Energy" )
	self:NetworkVar( "Int", max_energy_id, "MaxEnergy" )
	self:NetworkVar( "Int", fuel_id, "Fuel" )
end

function ENT:SetupDataTables()
	self:SetupDurability( 0, 1 )
	self:SetupEnergy( 2, 3, 4 )
end

if SERVER then
	function ENT:Initialize()
		self:SetModel( self.Model )
		
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self.MaxDurability )

		self:SetFuel( self.MaxFuel )
		self:SetMaxEnergy( self.MaxEnergy )

		self.NextGeneration = CurTime() + 1
	end

	function ENT:Think()
		if CurTime() > self.NextGeneration then
			self:Generate()
			self.NextGeneration = CurTime() + 1
		end
	end

	function ENT:AddEnergy( energy )
		self:SetEnergy( math.Clamp( self:GetEnergy() + energy, 0, self.MaxEnergy ) )
	end

	function ENT:TakeEnergy( energy )
		self:SetEnergy( math.Clamp( self:GetEnergy() - energy, 0, self.MaxEnergy ) )
	end

	function ENT:Generate()
		if self:GetFuel() <= 0 then return end

		self:AddEnergy( self.EnergyPerSecond )
		self:SetFuel( self:GetFuel() - 1 )
	end 
else
	function ENT:EnergyGauge()
		self.EnergyGaugeTable = self.EnergyGaugeTable or {}

		self.EnergyGaugeTable.Type = "gauge"
		self.EnergyGaugeTable.Value = self:GetEnergy()
		self.EnergyGaugeTable.Maximum = self:GetMaxEnergy()
		self.EnergyGaugeTable.Icon = "icon16/lightning.png"
		self.EnergyGaugeTable.Text = string.format( "Energy: %d/%d",
			self:GetEnergy(), self:GetMaxEnergy() )

		return self.EnergyGaugeTable
	end

	function ENT:FuelGauge()
		self.FuelGaugeTable = self.FuelGaugeTable or {}

		self.FuelGaugeTable.Type = "gauge"
		self.FuelGaugeTable.Value = self:GetFuel()
		self.FuelGaugeTable.Maximum = self.MaxFuel
		self.FuelGaugeTable.Icon = "icon16/water.png"
		self.FuelGaugeTable.Text = string.format( "Fuel: %d/%d",
			self:GetFuel(), self.MaxFuel )

		return self.FuelGaugeTable
	end

	function ENT:GetPopover()
		return {
			self:DurabilityGauge(),
			self:EnergyGauge(),
			self:FuelGauge()
		}
	end
end