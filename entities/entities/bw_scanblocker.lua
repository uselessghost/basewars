AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_consumable"

ENT.PrintName = "Scan Blocker"

ENT.Model = Model( "models/items/car_battery01.mdl" )

if CLIENT then return end

function ENT:Consume( pl )
	if pl.ScanBlocker then
		pl:Notify( "You already have a scan blocker." )
		return false
	end

	pl:Notify( "Scan blocker enabled." )
	pl.ScanBlocker = true
end

hook.Add( "PlayerCanRaid", "ScanBlocker", function( pl, target )
	if target.ScanBlocker then
		pl:NotifyError( "The player's scan blocker stopped you from scanning them!" )
		target:Notify( string.format( "%s just tried to scan you, scan blocker disabled.", pl:Name() ) )

		target.RaidCooldown = CurTime() + GetConVarNumber( "bw_raid_cooldown" )
		target.ScanBlocker = false

		return false
	end
end )