AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_printer"

ENT.PrintName = "Diamond Printer"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 250

ENT.EnergyCost = 200
ENT.PrintInterval = 30
ENT.MaxPaper = 65
ENT.Upgrades = {
	{ Output = 800 },
	{ Cost = 194620, Output = 1600 },
	{ Cost = 291929, Output = 3200 },
	{ Cost = 437894, Output = 6400 },
	{ Cost = 656841, Output = 12800 }
}

ENT.Model = Model( "models/props_c17/consolebox01a.mdl" )
ENT.Color = Color( 100, 200, 255 )

ENT.Raidable = true