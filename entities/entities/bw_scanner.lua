AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"
ENT.InfoIcon = "icon16/transmit.png" 

ENT.PrintName = "Scanner"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 150

if SERVER then
	function ENT:Initialize()
		self:SetModel( "models/props_rooftop/satellitedish02.mdl" )
		
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self:GetMaxDurability() )
	end

	function ENT:Use( pl )
		pl:ConCommand( "bw_scan" )
	end
end