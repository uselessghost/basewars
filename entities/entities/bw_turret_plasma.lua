AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_turret"
ENT.PrintName = "Plasma Turret"

ENT.EnergyCost = 5

ENT.Spread = 0.15
ENT.Range = 512
ENT.Speed = 0.15
ENT.Upgrades = {
	{ Damage = 8 },
	{ Cost = 5000, Damage = 10 },
	{ Cost = 7500, Damage = 12 }
}

ENT.Tracer = "AR2Tracer"
ENT.MuzzleEffect = "MuzzleEffect"

ENT.FireSound = Sound( "Weapon_AR2.NPC_Single" )

ENT.MaxDurability = 100

ENT.Raidable = true