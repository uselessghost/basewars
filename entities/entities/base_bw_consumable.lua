AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"

ENT.Consumable = true

ENT.MaxDurability = 10

if SERVER then
	function ENT:Initialize()
		if self.Model then self:SetModel( self.Model ) end
		
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self:GetMaxDurability() )
	end

	function ENT:Use( pl )
		if self:Consume( pl ) ~= false then self:Remove() end
	end

	function ENT:Consume( pl )
	end
end