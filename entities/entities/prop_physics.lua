AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"

ENT.PrintName = "Prop"

if SERVER then
	local DamageScale = CreateConVar( "bw_props_damagescale", 0.25 )
	local MinDurability = CreateConVar( "bw_props_mindura", 500 )
	local MaxDurability = CreateConVar( "bw_props_maxdura", 1000 )

	function ENT:Initialize()
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )

		if IsValid( self:GetPhysicsObject() ) then
			self:GetPhysicsObject():Wake()
			self:SetMaxDurability( self:GetPhysicsObject():GetMass() )
		else
			self:SetMaxDurability( MinDurability:GetInt() )
		end

		self:SetMaxDurability( math.Clamp( self:GetMaxDurability(), MinDurability:GetInt(),
			MaxDurability:GetInt() ) )

		self:SetDurability( self:GetMaxDurability() )
	end	

	function ENT:OnTakeDamage( dmg )
		if not dmg:IsDamageType( DMG_BLAST ) then return end
		if not self:CheckRaider( dmg:GetAttacker() ) then return end

		dmg:ScaleDamage( DamageScale:GetFloat() )
		self:SetDurability( self:GetDurability() - dmg:GetDamage() )

		if self:GetDurability() <= 0 then
			self:Destroy( dmg )
		end
	end
end