AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"
ENT.PrintName = "Fuel"
ENT.InfoIcon = "icon16/water.png"

ENT.MaxDurability = 10

if SERVER then
	function ENT:Initialize()
		self:SetModel( "models/props_junk/gascan001a.mdl" )

		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )
		self:SetUseType( SIMPLE_USE )

		self:SetCollisionGroup( COLLISION_GROUP_WEAPON )

		self:GetPhysicsObject():Wake()

		self:SetMaxDurability( self.MaxDurability )
		self:SetDurability( self:GetMaxDurability() )
	end

	function ENT:PhysicsCollide( col )
		if self.Used then return end

		local ent = col.HitEntity
		if not IsValid( ent ) or not ent.Generator then return end

		self:RefuelGenerator( ent )
		self:Remove()

		self.Used = true
	end

	function ENT:RefuelGenerator( ent )
		ent:EmitSound( "ambient/water/water_spray1.wav" )
		ent:SetFuel( ent.MaxFuel )
	end
end