AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_ammo"

ENT.PrintName = "Shotgun Ammo"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/Items/BoxBuckshot.mdl" )

ENT.AmmoType = "buckshot"
ENT.Amount = 16