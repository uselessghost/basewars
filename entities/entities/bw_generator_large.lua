AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_generator"

ENT.PrintName = "Large Generator"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.Model = Model( "models/props_c17/furnitureboiler001a.mdl" )

ENT.MaxDurability = 1000

ENT.MaxEnergy = 10000
ENT.EnergyPerSecond = 10