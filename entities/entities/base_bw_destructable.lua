AddCSLuaFile()

ENT.Type = "anim"
ENT.Destructable = true

function ENT:SetupDurability( dura_id, max_dura_id )
	self:NetworkVar( "Int", dura_id, "Durability" )
	self:NetworkVar( "Int", max_dura_id, "MaxDurability" )
end

function ENT:SetupDataTables()
	self:SetupDurability( 0, 1 )
end

if SERVER then
	local IllegalRaids = GetConVar( "bw_raid_illegalraids" )

	function ENT:SpawnFunction( pl, tr, class )
		local ent = ents.Create( class )
		ent:SetPos( tr.HitPos + tr.HitNormal * 16 )
		ent:Spawn()

		return ent
	end

	function ENT:Initialize()
		self:PhysicsInit( SOLID_VPHYSICS )
		self:SetSolid( SOLID_VPHYSICS )
		self:SetMoveType( MOVETYPE_VPHYSICS )

		self:GetPhysicsObject():Wake()
	end	

	function ENT:CheckRaider( pl )
		if IllegalRaids:GetBool() then return true end

		local creator = self:GetCreator()
		if not IsValid( pl ) or not IsValid( creator ) then return true end

		if pl == creator or creator:IsEnemy( pl ) then
			return true end

		return false
	end
	
	function ENT:OnTakeDamage( dmg )
		if not self:CheckRaider( dmg:GetAttacker() ) then return end

		self:SetDurability( self:GetDurability() - dmg:GetDamage() )

		if self:GetDurability() <= 0 then self:Destroy( dmg ) end
	end

	function ENT:Destroy( dmg )
		local ef = EffectData()
		ef:SetOrigin( self:GetPos() )
		util.Effect( "Explosion", ef, true, true )

		gamemode.Call( "EntityDestroyed", self, dmg )
		self:OnDestroyed()

		self:Remove()
	end

	function ENT:OnDestroyed( dmg )
		-- override
	end


	function ENT:Upgrade( pl )
		if self:GetLevel() >= #self.Upgrades then
			pl:NotifyError( "This entity is already at maximum level!" ) return end

		if not pl:CanAfford( self.Upgrades[ self:GetLevel() + 1 ].Cost ) then
			pl:NotifyError( "You don't have enough money to upgrade this entity." ) return end

		pl:TakeMoney( self.Upgrades[ self:GetLevel() + 1 ].Cost )

		self:SetLevel( self:GetLevel() + 1 )
		self:EmitSound( "buttons/button4.wav" )

		pl:Notify( string.format( "You upgraded your entity to level %d.", self:GetLevel() ) )
	end
else
	function ENT:LevelGauge()
		self.LevelGaugeTable = self.LevelGaugeTable or {}

		self.LevelGaugeTable.Type = "gauge"
		self.LevelGaugeTable.Value = self:GetLevel() - 1
		self.LevelGaugeTable.Maximum = #self.Upgrades - 1
		self.LevelGaugeTable.Icon = "icon16/star.png"
		self.LevelGaugeTable.Text = string.format( "Level %d", self:GetLevel() )

		if self:GetLevel() < #self.Upgrades then
			self.LevelGaugeTable.Text = self.LevelGaugeTable.Text .. string.format( " ($%s to upgrade)",
				string.Comma( self.Upgrades[ self:GetLevel() + 1 ].Cost ) )
		end

		return self.LevelGaugeTable
	end

	function ENT:DurabilityGauge()
		self.DurabilityGaugeTable = self.DurabilityGaugeTable or {}

		self.DurabilityGaugeTable.Type = "gauge"
		self.DurabilityGaugeTable.Value = self:GetDurability()
		self.DurabilityGaugeTable.Maximum = self:GetMaxDurability()
		self.DurabilityGaugeTable.Icon = "icon16/heart.png"
		self.DurabilityGaugeTable.Text = string.format( "Durability: %d/%d",
			self:GetDurability(), self:GetMaxDurability() )

		return self.DurabilityGaugeTable
	end

	function ENT:GetPopover()
		return { self:DurabilityGauge() }
	end
end