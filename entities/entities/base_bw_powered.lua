AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_destructable"

ENT.EnergyRange = 512

if SERVER then
	function ENT:GetNearbyGenerators()
		local generators = {}

		for k, v in ipairs( ents.FindInSphere( self:GetPos(), self.EnergyRange ) ) do
			if v.Generator and self:GetCreator() == v:GetCreator() then
				table.insert( generators, v ) end
		end

		return generators
	end

	function ENT:GetEnergy()
		local energy = 0

		for k, v in ipairs( self:GetNearbyGenerators() ) do
			energy = energy + v:GetEnergy()
		end

		return energy
	end

	function ENT:HasEnergy( energy )
		return self:GetEnergy() >= energy
	end

	function ENT:ConsumeEnergy( energy )
		for k, v in ipairs( self:GetNearbyGenerators() ) do
			local take = math.min( energy, v:GetEnergy() )
			
			v:TakeEnergy( take )
			energy = energy - take

			if energy <= 0 then return true end
		end

		return false
	end
end