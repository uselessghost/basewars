AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_printer"

ENT.PrintName = "Silver Printer"
ENT.Category = "BaseWars"

ENT.Spawnable = true
ENT.AdminOnly = true

ENT.MaxDurability = 150

ENT.EnergyCost = 60
ENT.PrintInterval = 50
ENT.MaxPaper = 55
ENT.Upgrades = {
	{ Output = 200 },
	{ Cost = 7594, Output = 400 },
	{ Cost = 11391, Output = 800 },
	{ Cost = 17086, Output = 1600 },
	{ Cost = 25629, Output = 3200 }
}

ENT.Color = Color( 235, 255, 255 )