AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_bw_turret"
ENT.PrintName = "Basic Turret"

ENT.EnergyCost = 1

ENT.Spread = 0.15
ENT.Range = 512
ENT.Speed = 0.20
ENT.Upgrades = {
	{ Damage = 4 },
	{ Cost = 1000, Damage = 6 },
	{ Cost = 2500, Damage = 8 }
}

ENT.Tracer = "Tracer"
ENT.MuzzleEffect = "MuzzleEffect"

ENT.FireSound = Sound( "NPC_FloorTurret.Shoot" )

ENT.MaxDurability = 100

ENT.Raidable = true