# README

A BaseWars gamemode for Garry's Mod. Currently has most of the systems you'd expect from a typical BaseWars gamemode but it's still missing some stuff.

# Prerequisites

Though technically speaking the gamemode itself functions as a standalone entity, the gamemode is configured by default to use my Lite Weapons Pack addon.
It's available here: http://steamcommunity.com/sharedfiles/filedetails/?id=536338229
If you want to use a different weapons pack, you can add them to gamemode/settings/buyables.lua and delete the existing lite entries already there.

# TODO

See Trello: https://trello.com/b/5xx12hL2

# Contributing

If you're gonna submit a pull request please adhere to the following coding guidelines.

* Source / Garry's Mod style CamelCase function names and class names.
* Parenthesis are padded with a single space, ie: Class:FunctionCall( some_argument )
* Ideally no more than 100 characters per line

Basically try not to write stuff that's too different from the code that's already there.